import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

typedef void PushNotificationClick(String payload);

class BaseConfig extends Object {
  static String? APP_NAME;

  static void setAppName(String appName) {
    APP_NAME = appName;
  }

  static String? APP_VERSION_NAME;

  static void setAppVersionName(String appVersionName) {
    APP_VERSION_NAME = appVersionName;
  }

  static int? APP_VERSION_CODE;

  static void setAppVersionCode(int appVersionCode) {
    APP_VERSION_CODE = appVersionCode;
  }

  static String? OPERATING_SYSTEM;

  static void setOperatingSystem(String operatingSystem) {
    OPERATING_SYSTEM = operatingSystem;
  }

  static String? OPERATING_SYSTEM_VERSION;

  static void setOperatingSystemVersion(String operatingSystemVersion) {
    OPERATING_SYSTEM_VERSION = operatingSystemVersion;
  }

  static String? DEVICE_MANUFACTURER;

  static void setDeviceManufacturer(String deviceManufacturer) {
    DEVICE_MANUFACTURER = deviceManufacturer;
  }

  static String? DEVICE_MODEL;
  static String? DEVICE_ID;

  static void setDeviceModel(String deviceModel) {
    DEVICE_MODEL = deviceModel;
  }
  static void setDeviceId(String deviceId) {
    DEVICE_ID = deviceId;
  }
  static void setUserId(int userId) {
    USER_ID = userId;
  }

  static String BASE_API_URL = "";

  static void setBaseApiUrl(String baseApiUrl) {
    BASE_API_URL = baseApiUrl;
  }

  static String BASE_PATH_URL = "";

  static void setBasePathUrl(String basePathUrl) {
    BASE_PATH_URL = basePathUrl;
  }

  static List<Locale> SUPPORTED_LOCALES = [];

  static void setSupportedLocales(List<Locale> supportedLocales) {
    SUPPORTED_LOCALES = supportedLocales;
  }

  static late Locale SELECTED_LOCALE;

  static void setSelectedLocale(Locale selectedLocale) {
    SELECTED_LOCALE = selectedLocale;
  }

  static late PushNotificationClick ON_PUSH_NOTIFICATION_CLICKED;

  static void setOnPushNotificationClicked(
      PushNotificationClick onPushNotificationClicked) {
    ON_PUSH_NOTIFICATION_CLICKED = onPushNotificationClicked;
  }

  static void onPushNotificationClicked(String payload) {
    ON_PUSH_NOTIFICATION_CLICKED(payload);
  }

  static double DEVICE_HEIGHT = 0;

  static void setDeviceHeight(double deviceHeight) {
    DEVICE_HEIGHT = deviceHeight;
  }

  static double DEVICE_WIDTH = 0;

  static void setDeviceWidth(double deviceWidth) {
    DEVICE_WIDTH = deviceWidth;
  }

  static bool IS_LANDSCAPE = false;

  static void setLandscape(bool isLandscape) {
    IS_LANDSCAPE = isLandscape;
  }

  static bool IS_CONNECTED = false;

  static void setIsConnected(bool isConnected) {
    IS_CONNECTED = isConnected;
  }

  static bool IS_GMS_AVAILABLE = false;

  static void setIsGmsAvailable(bool isGmsAvailable) {
    IS_GMS_AVAILABLE = isGmsAvailable;
  }

  static bool IS_HMS_AVAILABLE = false;

  static void setIsHmsAvailable(bool isHmsAvailable) {
    IS_HMS_AVAILABLE = isHmsAvailable;
  }
  static void setLogout(Function? logout) {
    LOGOUT = logout;
  }
  static void setLogoutFunction(Function? logout) {
    LOGOUT_FUNCTION = logout;
  }

  static Logger? logger;
  static void setLogger(Logger? l) {
    logger = l == null ? getDefaultLogger() : l;
  }

  static Logger? loggerScreen;
  static void setLoggerScreen(Logger? l) {
    loggerScreen = l == null ? getDefaultLogger() : l;
  }

  static Logger getDefaultLogger(){
    print("getDefaultLogger");
    return Logger(
      filter: null, // Use the defause the PrettyPrinter to format and print log
      output: null,
      printer: PrefixPrinter(

          PrettyPrinter(
              methodCount: 0,
              // number of method calls to be displayed
              errorMethodCount: 8,
              // number of method calls if stacktrace is provided
              lineLength: 120,
              // width of the output
              colors: false,
              // Colorful log messages
              printEmojis: false,
              // Print an emoji for each log message
              printTime: false // Should each log print contain a timestamp
          )),
    );
  }

  static int USER_ID = -1;
  static Function? LOGOUT;
  static Function? LOGOUT_FUNCTION;
  static void init(
      {required String appName,
      required String appVersionName,
        int appVersionCode = 0,
      required String baseApiUrl,
      required String basePathUrl,
      required List<Locale> supportedLocales,
      required Locale selectedLocale,
      required PushNotificationClick onPushNotificationClicked,
      required double deviceHeight,
      required double deviceWidth,
      required bool isLandscape,
      required bool isConnected,
      required Function? logout,
      required Function? logoutFunction,
      bool isGmsAvailable = false,
      bool isHmsAvailable = false,
      String operatingSystem = "",
      String operatingSystemVersion = "",
      String deviceManufacturer = "",
      String deviceModel = "",
      String deviceId = "",
        Logger? logger,
        Logger? loggerScreen
      }) {
    setAppName(appName);
    setAppVersionName(appVersionName);
    setAppVersionCode(appVersionCode);
    setBaseApiUrl(baseApiUrl);
    setBasePathUrl(basePathUrl);
    setSupportedLocales(supportedLocales);
    setSelectedLocale(selectedLocale);
    setOnPushNotificationClicked(onPushNotificationClicked);
    setDeviceHeight(deviceHeight);
    setDeviceWidth(deviceWidth);
    setLandscape(isLandscape);
    setIsConnected(isConnected);
    setIsHmsAvailable(isHmsAvailable);
    setIsGmsAvailable(isGmsAvailable);
    setOperatingSystem(operatingSystem);
    setOperatingSystemVersion(operatingSystemVersion);
    setDeviceManufacturer(deviceManufacturer);
    setDeviceModel(deviceModel);
    setDeviceId(deviceId);
    setLogout(logout);
    setLogoutFunction(logoutFunction);
    setLogger(logger);
    setLoggerScreen(loggerScreen);
  }
}

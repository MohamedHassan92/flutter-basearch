library base_arch;

import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:get/get.dart' hide Response, FormData, MultipartFile;
import 'package:dio_http_formatter/dio_http_formatter.dart';
import 'dart:typed_data';
import 'dart:ui';
import 'package:hidden_drawer_menu/hidden_drawer_menu.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:pinch_zoom/pinch_zoom.dart';
import '../modules/facebook_login/model/facebook_user.dart';
import '../modules/google_login/model/google_user.dart';
import '../widgets/pdf_viewer_local.dart';
import 'package:better_player/better_player.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:just_audio/just_audio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:video_player/video_player.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:platform_device_id/platform_device_id.dart';

import '../config/base_arch_config.dart';
import '../network_helper/base_arch_apis.dart';
import 'package:dio/dio.dart';
import 'package:dio_cache_interceptor/dio_cache_interceptor.dart';
import 'package:easy_localization/easy_localization.dart' hide TextDirection;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sprintf/sprintf.dart';
import 'package:url_launcher/url_launcher.dart';

import '../helpers/notifications/model/fcm_end_point_body.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:wakelock/wakelock.dart';

import 'package:device_info/device_info.dart';
import 'dart:core';
import 'package:intercom_flutter/intercom_flutter.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

//helper
part '../helpers/device_utils.dart';
part '../helpers/utils.dart';
part '../helpers/intercom_helper.dart';
part '../helpers/duration_utils.dart';
part '../helpers/url_launcher_utils.dart';
part '../helpers/dialog_utils.dart';
part '../helpers/notifications/cm_utils.dart';
part '../helpers/notifications/fcm_utils.dart';
part '../helpers/notifications/hcm_utils.dart';
part '../helpers/analytics/analytics_utils.dart';
part '../helpers/analytics/analytics_ecommerce_utils.dart';
part '../helpers/analytics/screen_report/route_aware_analytics.dart';
part '../helpers/analytics/screen_report/analytics_route_model.dart';
part '../helpers/notifications/local_notifications_utils.dart';
part '../helpers/notifications/full_screen_alert.dart';
//values
part '../values/base_arch_c.dart';
part '../values/base_arch_d.dart';
part '../values/base_arch_s.dart';
// network helper
part '../network_helper/base_arch_dio_utils.dart';
// widgets
part '../widgets/base/base_screen.dart';
part '../widgets/web_viewer_android.dart';
part '../widgets/pdf_viewer.dart';
part '../widgets/image_viewer/image_view_multi.dart';
part '../widgets/image_viewer/image_viewer_single.dart';
part '../widgets/loading_progress.dart';
part '../widgets/loading_progress_inside_view.dart';
part '../widgets/no_data_found.dart';
part '../widgets/text_field_with_borders.dart';
part '../widgets/app_bar_custom.dart';
part '../widgets/transition_image.dart';
part '../widgets/video_player_android.dart';
part '../widgets/video_player_ios.dart';
part '../widgets/spinner.dart';
part '../widgets/rating_bar.dart';
part '../widgets/drop_down_with_borders.dart';
//facebook_login
part '../modules/facebook_login/facebook_button.dart';

//google_login
part '../modules/google_login/google_button.dart';


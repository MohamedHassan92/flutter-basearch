part of base_arch;

typedef void GoogleLoginSuccessCallBack(GoogleUserModel GoogleUser);
typedef void GoogleLoginFailedCallBack(String error);
typedef void GoogleLoginCancelledCallBack();
typedef void GoogleLoginOnPressedCallBack();

class GoogleButton extends StatefulWidget {
  final GoogleLoginSuccessCallBack? onLoginSuccess;
  final GoogleLoginFailedCallBack? onLoginFailed;
  final GoogleLoginCancelledCallBack? onLoginCancelled;
  final GoogleLoginOnPressedCallBack? onLoginPressed;
  final double? height;
  final ShapeBorder? shape;
  final Color? color;
  final String? loginGoogleText;
  final TextStyle? textStyle;
  GoogleButton({@required this.onLoginSuccess,@required this.onLoginFailed,@required this.onLoginCancelled,this.height,this.color,this.loginGoogleText,this.shape,this.textStyle,@required this.onLoginPressed});

  @override
  State<GoogleButton> createState() => _GoogleButtonState();
}

class _GoogleButtonState extends State<GoogleButton> {

  late GoogleSignIn _googleSignIn;

  @override
  void initState(){
    super.initState();
    _googleSignIn = GoogleSignIn(
      // Optional clientId
      // clientId: '479882132969-9i9aqik3jfjd7qhci1nqf0bm2g71rm1u.apps.googleusercontent.com',
      scopes: <String>[
        'email',
        'https://www.googleapis.com/auth/userinfo.profile',
      ],
    );
  }

  Widget buildButton() {
    return
      ButtonTheme(
        minWidth: double.infinity,
        height: widget.height??BaseD.default_40,
        child: RaisedButton(
            shape: widget.shape??RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side:  BorderSide(color:Color.fromRGBO(46,89,132,1.0),width: 1)

            ),
            onPressed: () async {
              widget.onLoginPressed!();
              if(await _googleSignIn.isSignedIn()) await _googleSignIn.signOut();
              try {
                var user = await _googleSignIn.signIn();
                if(user != null){
                  GoogleUserModel googleUser = GoogleUserModel(
                    id: user.id,
                    email: user.email,
                    name: user.displayName,
                    firstName: user.displayName!.split(" ")[0],
                    lastName: user.displayName!.split(" ").length > 1 ? user.displayName!.split(" ")[1] : "",
                    picture: user.photoUrl,
                  );

                  widget.onLoginSuccess!(googleUser);
                }else{
                  widget.onLoginCancelled!();
                }
              } catch (error) {
                BaseConfig.logger!.v(error);
                widget.onLoginFailed!("error : ${error.toString()}");
              }
            },
            color: widget. color??Colors.white,
            child:
            Text( widget.loginGoogleText??"Login With Google", style: widget.textStyle??BaseS.h3(color:Color.fromRGBO(46,89,132,1.0)))


        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    return buildButton();
  }
}

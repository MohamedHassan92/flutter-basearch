class GoogleUserModel {
  String? id;
  String? email;
  String? name;
  String? firstName;
  String? lastName;
  String? picture;

  GoogleUserModel(
      {this.id,
        this.email,
        this.name,
        this.firstName,
        this.lastName,
        this.picture});


  GoogleUserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    name = json['name'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    picture = json['picture'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['name'] = this.name;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['picture'] = this.picture;
    return data;
  }
}

part of base_arch;

typedef void FacebookLoginSuccessCallBack(FacebookUserModel facebookUser);
typedef void FacebookLoginFailedCallBack(String error);
typedef void FacebookLoginCancelledCallBack();
typedef void FacebookLoginOnPressedCallBack();

class FacebookButton extends StatefulWidget {
  final FacebookLoginSuccessCallBack? onLoginSuccess;
  final FacebookLoginFailedCallBack? onLoginFailed;
  final FacebookLoginCancelledCallBack? onLoginCancelled;
  final FacebookLoginOnPressedCallBack? onLoginPressed;
  final double? height;
  final ShapeBorder? shape;
  final Color? color;
  final String? loginFaceBookText;
  final TextStyle? textStyle;

  FacebookButton({@required this.onLoginSuccess,@required this.onLoginFailed,@required this.onLoginCancelled,this.height,this.color,this.loginFaceBookText,this.shape,this.textStyle,@required this.onLoginPressed});

  @override
  State<FacebookButton> createState() => _FacebookButtonState();
}

class _FacebookButtonState extends State<FacebookButton> {
  Widget buildButton() {
     return

       ButtonTheme(
         minWidth: double.infinity,
         height: widget.height??BaseD.default_40,
         child: RaisedButton(
             shape:  widget.shape??RoundedRectangleBorder(
                 borderRadius: BorderRadius.circular(20),
                 side:  BorderSide(color:Color.fromRGBO(46,89,132,1.0),width: 1)

             ),
             onPressed: () async {
               widget.onLoginPressed!();
               await FacebookAuth.instance.logOut();
               final result = await FacebookAuth.instance
                   .login(permissions: ['email']);
               switch (result.status) {
                 case LoginStatus.success:
                 // get the user data
                   final userData = await FacebookAuth.instance.getUserData(
                       fields: "name,first_name,last_name,email,picture.width(500)");
                   // final userData = await _fb.getUserData(fields:"email,birthday");
                   var _userData = userData;
                   FacebookUserModel facebookUser = FacebookUserModel(
                     id: _userData['id'],
                     email: _userData.containsKey("email") ? _userData['email'] : "${_userData['id']}@facebook.com",
                     name: _userData['name'],
                     firstName: _userData['firstName'],
                     lastName: _userData['lastName'],
                     picture: _userData['picture']['data']['url'],
                   );

                   widget.onLoginSuccess!(facebookUser);

                   break;
                 case LoginStatus.cancelled:
                   BaseConfig.logger!.v("login cancelled");

                   widget.onLoginCancelled!();

                   break;
                 default:
                   BaseConfig.logger!.v("login failed");

                   widget.onLoginFailed!("error : ${result.message}");
               }
             },
             color: widget. color??Colors.white,
             child:
             Text( widget.loginFaceBookText??"Login With Facebook", style: widget.textStyle??BaseS.h3(color:Color.fromRGBO(46,89,132,1.0)))

         ),
       );
  }

  @override
  Widget build(BuildContext context) {
    return buildButton();
  }
}

class BaseApis extends Object {
  static const SINGLE = 1;
  static const LIST = 2;

  static const CODE_SUCCESS = 200;
  static const CODE_ERROR = 500;

  static const REQ_SUCCESS = "success";
  static const REQ_NOT_ALLOWED = "NotAllowed";
  static const REQ_DATA_RETURNED = "DataReturn";
  static const REQ_EMPTY_DATA = "EmptyData";
  static const REQ_FAILED = "failed";
  static const ExCEPTION = "Exception";

  static var TOKEN_VALUE = "NDMwMTJmMjItMzRjNi00ZjVkLTg5YWYtNTU3N2M5YTdlZTdi";
}

part of base_arch;

class BaseDioUtils {
  static Dio? dio;
  static Options? authOptions;
  static CacheOptions cacheOptions =
      CacheOptions(store: MemCacheStore(), policy: CachePolicy.noCache);

  static const int CONNECTION_TIME_OUT = 60000;

  static const String REQUEST_GET = "get";
  static const String REQUEST_POST = "post";
  static const String REQUEST_PUT = "put";
  static const String REQUEST_DELETE = "delete";

  static bool isUnAuthorizedDialogShown = false;

  static void initDio({bool reset = false}) {
    if (dio == null || reset) {
      dio = Dio(BaseOptions(connectTimeout: CONNECTION_TIME_OUT));
      dio!.interceptors.add(DioCacheInterceptor(options: cacheOptions));
      dio!.interceptors.add(HttpFormatter(logger: BaseConfig.loggerScreen));
      dio!.interceptors.add(HttpFormatter());
      dio!.interceptors.add(InterceptorsWrapper(onRequest:
          (RequestOptions options, RequestInterceptorHandler handler) async {
        options.queryParameters['appname'] = BaseConfig.APP_NAME;
        options.queryParameters['version'] = BaseConfig.APP_VERSION_NAME;

        return handler.next(options);
      }, onResponse:
          (Response response, ResponseInterceptorHandler handler) async {
        return handler.next(response);
      }, onError: (DioError e, ErrorInterceptorHandler handler) async {
        if (e.response?.statusCode == 401) {
          BaseConfig.logger!.v("*****error 401 ");
          await BaseConfig.LOGOUT_FUNCTION!();
          if (!isUnAuthorizedDialogShown) {
            isUnAuthorizedDialogShown = true;
            Get.defaultDialog(
                title: "",
                barrierDismissible: false,
                titleStyle: const TextStyle(fontSize: 0),
                titlePadding: EdgeInsets.zero,
                content: Text(tr("login_again")),
                textConfirm: tr("logout"),
                onConfirm: () => BaseConfig.LOGOUT!());
          }
          return handler.next(e);
        } else {
          return handler.next(e);
        }
      }));
    }
    authOptions = Options(headers: {
      'authorization': 'Bearer ${BaseApis.TOKEN_VALUE}',
    });
  }

  static Dio? instance() {
    initDio();
    return dio;
  }

  static Future<Response?> request(
    String requestType,
    String url, {
    String? contentType,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
    dynamic body,
    Options? requestOption,
    bool isToCache = true,
    bool forceRefresh = true,
    int daysToCache = 30,
    int hoursToCache = 0,
  }) async {
    initDio();

    if (BaseApis.TOKEN_VALUE.isEmpty) {
      authOptions!.headers = {};
    }

    authOptions!.headers!["xAppVersion"] = BaseConfig.APP_VERSION_NAME;
    authOptions!.headers!["xOsVersion"] = Platform.operatingSystemVersion;
    authOptions!.headers!["xPlatform"] = Platform.operatingSystem;
    authOptions!.headers!['xOperatingSystem'] = BaseConfig.OPERATING_SYSTEM;
    authOptions!.headers!['xOperatingSystemVersion'] =
        BaseConfig.OPERATING_SYSTEM_VERSION;
    authOptions!.headers!['xDeviceManufacturer'] =
        Platform.isIOS ? "Apple" : BaseConfig.DEVICE_MANUFACTURER ?? "";
    authOptions!.headers!['xDeviceModel'] = BaseConfig.DEVICE_MODEL;
    authOptions!.headers!['xUserId'] = BaseConfig.USER_ID;
    authOptions!.headers!['xDeviceId'] = BaseConfig.DEVICE_ID;

    if (headers != null && headers.isNotEmpty) {
      headers.keys.forEach((key) {
        authOptions!.headers![key] = headers[key];
      });
    }

    if (contentType != null)
      authOptions!.headers![Headers.contentTypeHeader] = contentType;

    BaseConfig.logger!.v("Apis.TOKEN_VALUE ${BaseApis.TOKEN_VALUE}");
    BaseConfig.logger!.v("$url : headers : ${jsonEncode(authOptions!.headers)}");
    if (body != null) Utils.printLongLine("$url : body : ${body.toString()}");

    var options = isToCache
        ? cacheOptions
            .copyWith(
                policy: forceRefresh
                    ? CachePolicy.refreshForceCache
                    : CachePolicy.forceCache,
                maxStale: Duration(days: daysToCache, hours: hoursToCache))
            .toOptions()
        : authOptions;

    if (isToCache) {
      if (options!.headers == null) options.headers = {};
      options.headers!.addAll(authOptions!.headers!);
    }

    Response? response;

    try {
      switch (requestType) {
        case REQUEST_GET:
          response = await dio!.get(
            url,
            queryParameters: queryParameters,
            options: options,
          );
          break;
        case REQUEST_POST:
          response = await dio!.post(
            url,
            options: requestOption ?? options,
            queryParameters: queryParameters,
            data: body,
            onSendProgress: (int sent, int total) =>
                BaseConfig.logger!.v("$url : sent : $sent/$total"),
            onReceiveProgress: (recieved, total) =>
                BaseConfig.logger!.v("$url : recieved : $recieved/$total"),
          );
          break;
        case REQUEST_PUT:
          response = await dio!.put(
            url,
            options: options,
            queryParameters: queryParameters,
            data: body,
            onSendProgress: (int sent, int total) =>
                BaseConfig.logger!.v("$url : sent : $sent/$total"),
            onReceiveProgress: (recieved, total) =>
                BaseConfig.logger!.v("$url : recieved : $recieved/$total"),
          );
          break;
        case REQUEST_DELETE:
          response = await dio!.delete(
            url,
            options: options,
            queryParameters: queryParameters,
            data: body,
          );
          break;
      }
    } catch (e) {
      var error = e as DioError;
      if (!isRetryDialogShown &&
          (e.type == DioErrorType.connectTimeout ||
              e.type == DioErrorType.sendTimeout ||
              e.type == DioErrorType.receiveTimeout)) {
        try {
          AnalyticsUtils.logEvent("server_timeout", parameters: {
            "url": e.requestOptions.path,
          });
        }catch(error){

        }
        response = await retryRequest(error);
      } else {
        response = e.response;
      }
    }

    //printResponse(url, response);

    return response;
  }

  static printResponse(String url, Response? response) {
    if (response != null) {
      BaseConfig.logger!.v("$url : ${response.statusCode} ${response.data}");
    } else {
      BaseConfig.logger!.v("$url : response == null");
    }
  }

  static bool isRetryDialogShown = false;

  static Future<Response?> retryRequest(DioError e) async {
    isRetryDialogShown = true;
    bool isToRetry = false;

    await Get.defaultDialog(
        title: "",
        titleStyle: const TextStyle(fontSize: 0),
        titlePadding: EdgeInsets.zero,
        contentPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        middleText: tr("try_again"),
        textConfirm: tr("try_again_btn"),
        onConfirm: () {
          isRetryDialogShown = false;
          isToRetry = true;
          Get.back();
        },
        textCancel: tr("cancel"));

    isRetryDialogShown = false;

    if (!isToRetry) return e.response;

    initDio(reset: true);
    dio!.options.connectTimeout = CONNECTION_TIME_OUT;

    RequestOptions requestOptions = e.requestOptions;
    if (requestOptions.data is FormData) {
      FormData formData = FormData();
      formData.fields.addAll(requestOptions.data.fields);

      for (MapEntry mapFile in requestOptions.data.files) {
        formData.files.add(MapEntry(
            mapFile.key,
            MultipartFile.fromFileSync(mapFile.value.FILE_PATH,
                filename: mapFile.value.filename)));
      }
      requestOptions.data = formData;
    }

    Response? response;
    try {
      response = await dio?.request(
        e.requestOptions.path,
        cancelToken: e.requestOptions.cancelToken,
        data: e.requestOptions.data,
        onReceiveProgress: e.requestOptions.onReceiveProgress,
        onSendProgress: e.requestOptions.onSendProgress,
        queryParameters: e.requestOptions.queryParameters,
        options: requestOptions.asOptions(),
      );
    } catch (e) {
      var error = e as DioError;
      if (!isRetryDialogShown &&
          (e.type == DioErrorType.connectTimeout ||
              e.type == DioErrorType.sendTimeout ||
              e.type == DioErrorType.receiveTimeout)) {
        response = await retryRequest(error);
      } else {
        response = e.response;
      }
    }

    return response;
  }

  static clearCache() {
    dio?.interceptors.clear();
  }
}

extension _AsOptions on RequestOptions {
  Options asOptions() {
    return Options(
      method: method,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
      extra: extra,
      headers: headers,
      responseType: responseType,
      contentType: contentType,
      validateStatus: validateStatus,
      receiveDataWhenStatusError: receiveDataWhenStatusError,
      followRedirects: followRedirects,
      maxRedirects: maxRedirects,
      requestEncoder: requestEncoder,
      responseDecoder: responseDecoder,
      listFormat: listFormat,
    );
  }
}


part of base_arch;

class WebViewerAndroid extends StatefulWidget {
  String url, asset;
  bool isDrawerEnabled;
  bool isBackEnabled;

  WebViewerAndroid({this.url = "", this.asset = "", this.isDrawerEnabled = true,this.isBackEnabled = false});

  @override
  _WebViewerAndroidState createState() => _WebViewerAndroidState();
}

class _WebViewerAndroidState extends State<WebViewerAndroid> {
  var TAG = "_WebViewerAndroidState";

  @override
  void initState() {
    super.initState();
    widget.url = widget.url.replaceAll("http:", "https:");
  }

  bool isLoading = true;

  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return BaseScreen(TAG,
        isLoading: isLoading,
        isBackEnabled: widget.isDrawerEnabled,
        body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Stack(children: [
            Container(
                height: double.infinity,
                width: double.infinity,
                child: WebView(
                  initialUrl: "",
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (WebViewController controller) {
                    if (widget.asset.isNotEmpty) {
                      _loadHtmlFromAssets(controller);
                    } else if (widget.url.isNotEmpty) {
                      controller.loadUrl(widget.url);
                    }
                  },
                  onPageFinished: (url) {
                    BaseConfig.logger!.v(url);
                    setState(() => isLoading = false);
                  },
                )),
            widget.isBackEnabled
                ? Container(
                    margin: EdgeInsets.all(10.0),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Container(
                                margin: EdgeInsets.only(right: BaseD.default_5),
                                width: (shortestSide > 550)
                                    ? BaseD.default_80
                                    : BaseD.default_50,
                                height: (shortestSide > 550)
                                    ? BaseD.default_80
                                    : BaseD.default_50,
                                child:
                                    Image.asset("assets/images/close.png")))),
                  )
                : Container()
          ]),
        ));
  }

  _loadHtmlFromAssets(WebViewController controller) async {
    String fileHtmlContents = await rootBundle.loadString(widget.asset);
    controller.loadUrl(Uri.dataFromString(fileHtmlContents,
            mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
        .toString());
  }
}

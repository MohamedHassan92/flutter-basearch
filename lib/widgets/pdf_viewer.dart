part of base_arch;

class PdfViewer extends StatefulWidget {
  final String? url, path, title;
  final bool noAppbar;
  Widget? floatingActionButton;
  PdfViewer(this.title,
      {this.url, this.path, this.noAppbar = false,this.floatingActionButton});

  @override
  _PdfViewerState createState() => _PdfViewerState();
}

class _PdfViewerState extends State<PdfViewer> with RouteAwareAnalytics{

  @override
  AnalyticsRoute get route {
    return AnalyticsRoute("pdf_viewer_screen","$runtimeType");
  }

  String? pathPDF = "";
  bool isFileReady = false;
  double progress = 0.0;

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      Wakelock.enable();
    }
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      if (widget.path != null) {
        setState(() {
          pathPDF = widget.path;
          isFileReady = true;
        });
      } else if (widget.url != null) {
        downloadPdf();
      }
    });
  }

  downloadPdf() async {
    bool? ready = await _isStorageReady();
    if (ready!) {
      createFileOfPdfUrl();
    }
  }

  var appDir;

  Future<bool?> _isStorageReady() async {

    var status = await Permission.storage.status;
    if (!status.isGranted) {
      await Permission.storage.request().then((value) async {
        if( value.isDenied)
          Navigator.of(context).pop();


        if( value.isGranted)
        {
          if (Platform.isIOS) {
            appDir = await getApplicationDocumentsDirectory();
            return true;
          } else {
            appDir = await getExternalStorageDirectory();
            return true;
          }

        }
        setState(() {

        });

      });
      return true;

    }else {
      if (Platform.isIOS) {
        appDir = await getApplicationDocumentsDirectory();
        return true;
      } else {
        appDir = await getExternalStorageDirectory();
        return true;
      }

    }
  }

  createFileOfPdfUrl() async {
    final url = widget.url!;
    final filename = url.substring(url.lastIndexOf("/") + 1);
    file = await File('${appDir.path}/$filename');
    pathPDF = file.path;
    BaseConfig.logger!.v(pathPDF);

    Dio dio = Dio();

    CacheOptions cacheOptions = CacheOptions(store: MemCacheStore(), policy: CachePolicy.forceCache, maxStale: Duration(days: 365));

    dio.interceptors.add(DioCacheInterceptor(options: cacheOptions));
    var options = cacheOptions.toOptions();
    options.responseType = ResponseType.bytes;
    options.followRedirects = false;

    try {
      Response response = await dio.get(
        url,
        onReceiveProgress: (received, total) {
          if (total != -1) {
            BaseConfig.logger!.v("${received / total}%");
            setState(() {
              progress = received / total;
            });
          }
        },
        //Received data with List<int>
        options: options,
      );
      BaseConfig.logger!.v(response.headers);
      var raf = file.openSync(mode: FileMode.write);
      // response.data is List<int> type
      raf.writeFromSync(response.data);
      await raf.close();

      setState(() {
        isFileReady = true;
      });
    } catch (e) {
      BaseConfig.logger!.v(e);
    }
  }

  late File file;

  @override
  Widget build(BuildContext context) {

    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return BaseScreen(
        "",
        isBackEnabled: true,
        isFullScreen: false,
        isFloatingAppBarEnabled:true,
        isPdfViewer: true,
        floatingActionButton:widget.floatingActionButton,
        appBarCustom: getAppBar(),
        body: isFileReady
            ? Stack(
            alignment:Alignment.bottomLeft,
            children:[
              PdfViewerLocalUrl(file.path),

            ]
        )
            : Container(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(
                        value: progress,
                        valueColor: AlwaysStoppedAnimation(Colors.blue),
                        strokeWidth: 5.0),
                    height:BaseD.default_50,
                    width:BaseD.default_50,
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
              )
            ],
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
        ));
  }
  AppBar getAppBar() {
    return AppBar(
      automaticallyImplyLeading: false,

        flexibleSpace: Container(
          padding: EdgeInsets.only(top:BaseD.default_10),
          child: Align(child:InkWell(child: Icon(
          Icons.arrow_forward_ios_rounded,
          size: BaseD.default_30,
          color: Colors.white,
        ),onTap: (){
          Navigator.pop(context);
        },),alignment: AlignmentDirectional.centerEnd,),));
  }
}

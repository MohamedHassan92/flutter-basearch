part of base_arch;

class Spinner extends StatelessWidget {
  Spinner({this.hint,
    this.obsecure = false,
    this.validator,
    this.onSaved,
    this.onTap,
    this.keyboardType,
    this.controller,
    this.textStyle,
    this.maxLines = 1,
    this.minLines = 1,
    this.hintStyle});

  final FormFieldSetter<String>? onSaved;
  final String? hint;
  final bool obsecure;
  final TextEditingController? controller;
  final FormFieldValidator<String>? validator;
  final Function? onTap;
  final TextInputType? keyboardType;
  final TextStyle? textStyle;
  final TextStyle? hintStyle;
  final int maxLines;
  final int minLines;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: keyboardType == null ? TextInputType.text : keyboardType,
      controller: controller,
      onSaved: onSaved,
      onTap: onTap as void Function()?,
      validator: validator,
      autofocus: false,
      enableInteractiveSelection: onTap == null,
      focusNode: onTap != null ? FocusNode(canRequestFocus: false) : null,
      obscureText: obsecure,
      style: textStyle,
      maxLines: maxLines,
      minLines: minLines,
      decoration: InputDecoration(
        filled: true,
        isDense: false,
        alignLabelWithHint: true,
        contentPadding: EdgeInsets.symmetric(vertical: 3, horizontal: 20),
        errorMaxLines: 2,
        labelStyle: TextStyle(fontSize: 15, color: Colors.black12),
        labelText: hint,
        hintStyle: hintStyle,
        //suffixIcon: Icon(this.icon, color: Colors.black26,),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(
            color: Colors.grey,
            width: 2,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(
            color: Colors.white,
            width: 2,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(
            color: Colors.red,
            width: 2,
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(
            color: Colors.grey,
            width: 2,
          ),
        ),
      ),
    );
  }
}

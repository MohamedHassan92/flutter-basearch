part of base_arch;
typedef void onMenuClickCallBack(int position, SimpleHiddenDrawerController controller);

class BaseScreen extends StatefulWidget {
  String? title;
  bool isLoading;
  bool isTopLoadingEnabled;
  bool isPdfViewer;
  bool isBackEnabled;
  List<Widget>? actions;
  Widget? body;
  String TAG;
  double paddingH;
  double paddingV;
  Widget? floatingActionButton;
  Widget? loadingCustomDesign;
  bool isFloatingAppBarEnabled;
  bool isFullScreen;
  Function? onBackPressed;
  Drawer? drawer;
  Widget? bottomNavigationBar;
  Color? backgroundColor;
  AppBar? appBarCustom;
  Widget? menu;
  TypeOpen? typeOpen;
  bool? isDraggable;
  onMenuClickCallBack? onMenuClick;
  BaseScreen(this.TAG,
      {this.title,
        this.isLoading = false,
        this.isBackEnabled = true,
        this.isFloatingAppBarEnabled = true,
        this.body,
        this.actions,
        this.paddingH = 0,
        this.paddingV = 0,
        this.floatingActionButton,
        this.isFullScreen = false,
        this.isTopLoadingEnabled = false,
        this.onBackPressed,
        this.drawer,
        this.bottomNavigationBar,
        this.backgroundColor,
        this.appBarCustom,
        this.menu,
        this.onMenuClick,
        this.typeOpen,
        this.isDraggable,
        this.loadingCustomDesign,
        this.isPdfViewer=false,
      });

  @override
  _BaseScreenState createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    checkConnectivity();

    BaseConfig.logger!.v("title : ${widget.title}");

  }

  checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      BaseConfig.IS_CONNECTED = true;
    } else {
      showInternetErrorMsg();
    }
  }

  showInternetErrorMsg() {
    Flushbar(
      message: tr("no_connection"),
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Colors.red,
      flushbarStyle: FlushbarStyle.GROUNDED,
      duration: Duration(seconds: 3),
      borderRadius: BorderRadius.all(Radius.circular(BaseD.default_10)),
    )..show(context);
  }


  Future<bool> _onWillPop() {
    if( widget.onBackPressed==null){
      return Future.value(true);

    }else {
      widget.onBackPressed!();

      return Future.value(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child:SafeArea(
          child: Column(
            children: [
              Expanded(
                child:
widget.menu!=null?
                SimpleHiddenDrawer(
                  isDraggable:(widget.isDraggable??false),
                  typeOpen: widget.typeOpen??TypeOpen.FROM_LEFT,
                  menu: widget.menu??Container(),
                  screenSelectedBuilder: (int position, SimpleHiddenDrawerController controller) {
                  if(widget.menu!= null)  widget.onMenuClick!(position,controller);
                    return Scaffold(
                      key: _scaffoldKey,
                      drawer: widget.drawer,
                      backgroundColor: widget.backgroundColor,
                      appBar: widget.isFullScreen ? null : widget.isFloatingAppBarEnabled
                          ? null
                          : widget.appBarCustom,
                      floatingActionButton: widget.floatingActionButton,
                      bottomNavigationBar: widget.bottomNavigationBar,
                      body: Stack(
                        fit: StackFit.expand,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            height: double.infinity,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: widget.paddingH, vertical: widget.paddingV),
                              child: Column(
                                children: <Widget>[
                                  Expanded(child: !widget.isFullScreen &&
                                      widget.isFloatingAppBarEnabled ?
                                  Stack(
                                      children: [
                                        Container(margin: EdgeInsets.only(
                                            top: BaseD.default_50), child: widget.body),
                                        Column(
                                            children: [
                                              Container(
                                                  child: widget.appBarCustom
                                              ),
                                              widget.isTopLoadingEnabled && widget.isLoading
                                                  ? topProgress()
                                                  : Container()
                                            ]
                                        )
                                      ]
                                  ) : SafeArea(child: Column(
                                      children: [
                                        widget.isTopLoadingEnabled && widget.isLoading
                                            ? topProgress()
                                            : Container(),
                                        Expanded(child: widget.body!)
                                      ]
                                  ))),
                                ],
                              ),
                            ),
                          ),

                          !widget.isTopLoadingEnabled && widget.isLoading
                              ? widget.loadingCustomDesign!=null?widget.loadingCustomDesign!:LoadingProgress()
                              : Container()
                        ],
                      ),
                    ); },

                ):Scaffold(
          key: _scaffoldKey,
          drawer: widget.drawer,
          backgroundColor: widget.backgroundColor,
          appBar: widget.isFullScreen ? null : widget.isFloatingAppBarEnabled
              ? null
              : widget.appBarCustom,
      floatingActionButton: widget.floatingActionButton,
          bottomNavigationBar: widget.bottomNavigationBar,
          body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
      Container(
      width: double.infinity,
          height: double.infinity,
          child: Padding(
          padding: EdgeInsets.symmetric(
          horizontal: widget.paddingH, vertical: widget.paddingV),
      child: Column(
        children: <Widget>[
          Expanded(child: !widget.isFullScreen &&
              widget.isFloatingAppBarEnabled ?
          Stack(
              children: [
                Container(margin: EdgeInsets.only(
                    top: BaseD.default_50), child: widget.body),
                Column(
                    children: [
                      Container(
                           child: widget.appBarCustom
                      ),
                      widget.isTopLoadingEnabled && widget.isLoading
                          ? topProgress()
                          : Container()
                    ]
                )
              ]
          ) : SafeArea(child: Column(
              children: [
                widget.isTopLoadingEnabled && widget.isLoading
                    ? topProgress()
                    : Container(),
                Expanded(child: widget.body!)
              ]
          ))),
        ],
      ),
    ),
    ),

    !widget.isTopLoadingEnabled && widget.isLoading
    ? widget.loadingCustomDesign!=null?widget.loadingCustomDesign!:LoadingProgress()
        : Container()
    ],
    ),
    ),
              ),

            ],
          )
      )
    );
  }

  Widget topProgress() {
    return Container(
        padding: EdgeInsets.all(BaseD.default_5),
        color: Colors.amber,
        child: Row(
            children: [
              Expanded(
                  child: Text(tr("loading"), style: BaseS.h3(color: Colors.white))
              ),
              SizedBox(
                child: CircularProgressIndicator(
                  strokeWidth: 4,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      Colors.white),
                ),
                height: BaseD.default_20,
                width: BaseD.default_20,
              )
            ]
        )
    );
  }
}

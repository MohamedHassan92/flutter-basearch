part of base_arch;

class VideoPlayerIos extends StatefulWidget {
  VideoPlayerIos(this.videoUrl,{this.isOpenedForHelpVideo=false});

  String videoUrl;
  bool isOpenedForHelpVideo;
  @override
  _VideoPlayerIosState createState() => _VideoPlayerIosState();
}

class _VideoPlayerIosState extends State<VideoPlayerIos> {

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  late VideoPlayerController videoPlayerController;

  late ChewieController chewieController;

  late Widget playerWidget;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);



    widget.videoUrl = widget.videoUrl.replaceAll("http:","https:");

    videoPlayerController = VideoPlayerController.network(widget.videoUrl);

    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      aspectRatio: 16 / 9,
      autoPlay: true,allowFullScreen: false,
      allowPlaybackSpeedChanging: false,
      looping: false,
    );

    playerWidget = Chewie(
      controller: chewieController,
    );
  }


  @override
  void dispose() {
    super.dispose();
    videoPlayerController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: Stack(children: <Widget>[
        Container(
          height: double.infinity,
          alignment: Alignment.center,
          child: Container(
            alignment: Alignment.center,
            // margin: EdgeInsets.only(top: D.de),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                playerWidget,
              ],
            ),
          ),
        ),
      ]),
    );
  }
}

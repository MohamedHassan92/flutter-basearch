part of base_arch;

class LoadingProgressInsideView extends StatelessWidget {
  const LoadingProgressInsideView({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        BaseConfig.logger!.v("progress : onTap");
      },
      child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Center(
              child: Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: SizedBox(
                      child: CircularProgressIndicator(
                        strokeWidth: 6,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.lightBlueAccent),
                      ),
                      height: BaseD.default_70,
                      width: BaseD.default_70,
                    ),
                  )))),
    );
  }
}
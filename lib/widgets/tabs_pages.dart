import '../config/base_arch_library.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class TabsPagesWidget extends StatefulWidget {
  final List<Widget>? pages;
  final List<String>? tabsTexts;
  final bool isLandScape;
  bool isTapsScrollable;
  int selectedIndex;
  Color? tapsBgdColor = Colors.transparent;
  double? tapsWidth = double.infinity;
  Color? selectedColor;
  Color? unSelectedColor;
  TabsPagesWidget(
      {Key? key,
      this.pages,
      this.tabsTexts,
      this.tapsBgdColor,
      this.tapsWidth,
      this.selectedIndex = 0,
      this.isTapsScrollable=true, this.isLandScape = false,this.selectedColor,this.unSelectedColor})
      : super(key: key);

  @override
  _TabsPagesWidgetState createState() =>
      _TabsPagesWidgetState(tabsTexts, pages, tapsBgdColor, tapsWidth,
          selectedIndex: selectedIndex,isTapsScrollable: isTapsScrollable);
}

class _TabsPagesWidgetState extends State<TabsPagesWidget> {
  int selectedIndex;
  final _selectedKey = new GlobalKey();

  PageController? _pageController;
  ItemScrollController? _itemScrollController;

  final List<Widget>? pages;
  final List<String>? tabsTexts;
  final tapsBgdColor;

  final tapsWidth;
  final List<bool>? isPremium;
  bool isTapsScrollable;

  _TabsPagesWidgetState(
      this.tabsTexts, this.pages, this.tapsBgdColor, this.tapsWidth,
      {this.selectedIndex = 0, this.isPremium,this.isTapsScrollable=true});

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: selectedIndex);
    _itemScrollController = ItemScrollController();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }

  Orientation _orientation = Orientation.portrait;

  Widget build(BuildContext context) {
    if(_orientation!=MediaQuery.of(context).orientation)
      _pageController = PageController(initialPage: selectedIndex);
    _orientation = MediaQuery.of(context).orientation;

    _pageController = PageController(initialPage: selectedIndex);

    List<Widget> listTabs = createListOFTabs();
    return widget.isLandScape?Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(color: tapsBgdColor),
            child:Center(
              child: Container(
                  padding: EdgeInsets.all (BaseD.default_5),
                  child: ScrollablePositionedList.builder(
                    itemCount: listTabs.length,
                    itemBuilder: (context, index) => listTabs[index],
                    scrollDirection: Axis.vertical,
                    itemScrollController: _itemScrollController,
                    padding: EdgeInsets.zero,
                  )),
            )
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            margin: EdgeInsets.all (BaseD.default_10),
            child: PageView(
              controller: _pageController,
              children: pages!,
              onPageChanged: (index) {
                setState(() {
                  selectedIndex = index;
                });
                if(isTapsScrollable)
                  _itemScrollController!.scrollTo(
                    index: index,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeInCubic);
              },
            ),
          ),
        ),
      ],
    ):Column(
      children: <Widget>[
        Container(
          width: double.infinity,
          decoration: BoxDecoration(color: tapsBgdColor),
          child:isTapsScrollable? Center(
            child: Container(
                width: tapsWidth,
                padding: EdgeInsets.all (BaseD.default_5),
                height: BaseD.default_50,
                child: ScrollablePositionedList.builder(
                  itemCount: listTabs.length,
                  itemBuilder: (context, index) => listTabs[index],
                  scrollDirection: Axis.horizontal,
                  itemScrollController: _itemScrollController,
                  padding: EdgeInsets.zero,
                )),
          ):createStaticListOFTabs(),
        ),
        Expanded(
          flex: 1,
          child: Container(
            margin: EdgeInsets.all (BaseD.default_10),
            child: PageView(
              controller: _pageController,
              children: pages!,
              onPageChanged: (index) {
                setState(() {
                  selectedIndex = index;
                });
                if(isTapsScrollable)
                  _itemScrollController!.scrollTo(
                      index: index,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeInCubic);
              },
            ),
          ),
        ),
      ],
    );
  }

  List<Widget> createListOFTabs() {
    return tabsTexts!.asMap().entries.map((entry) {
      int index = entry.key;
      String text = entry.value;
      return Container(
        margin: EdgeInsets.symmetric(horizontal: BaseD.default_5),
        child: ButtonTheme(
          minWidth: BaseD.default_100,
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular (BaseD.default_25),
            ),
            splashColor: BaseC.skyBlue,
             color: selectedIndex == index ?widget.selectedColor??BaseC.mostardaBtn: widget.unSelectedColor??BaseC.skyBlueDark,
            key: selectedIndex == index ? _selectedKey : null,
            textColor: Colors.white,
            onPressed: () {
              setState(() {
                selectedIndex = index;
              });
              _pageController!.animateToPage(index,
                  duration: Duration(milliseconds: 500), curve: Curves.ease);
            },
            child:Text(
              text,
            ),
          ),
        ),
      );
    }).toList();
  }

  Widget createStaticListOFTabs() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
crossAxisAlignment:CrossAxisAlignment.center ,
        children: tabsTexts!.asMap().entries.map((entry) {
      int index = entry.key;
      String text = entry.value;
      return Expanded(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: BaseD.default_5),
          child: ButtonTheme(
            minWidth: BaseD.default_100,
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular (BaseD.default_25),
              ),
              splashColor: BaseC.skyBlue,
              color: selectedIndex == index ? BaseC.mostardaBtn : BaseC.skyBlueDark,
              key: selectedIndex == index ? _selectedKey : null,
              textColor: Colors.white,
              onPressed: () {
                setState(() {
                  selectedIndex = index;
                });
                _pageController!.animateToPage(index,
                    duration: Duration(milliseconds: 500), curve: Curves.ease);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment:CrossAxisAlignment.center ,
                children: [
                  Text(
                    text,
                    style: BaseS.h2(color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }).toList());
  }
}

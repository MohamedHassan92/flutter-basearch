part of base_arch;

class LoadingProgress extends StatelessWidget {
  const LoadingProgress({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        BaseConfig.logger!.v("progress : onTap");
      },
      child: Container(
          color: Colors.black38,
          width:  BaseConfig.DEVICE_WIDTH,
          height: BaseD.default_150,
          child: Center(
              child: Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: SizedBox(
                      child: CircularProgressIndicator(
                        strokeWidth: 6,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.lightBlueAccent),
                      ),
                      height: BaseD.default_70,
                      width: BaseD.default_70,
                    ),
                  )))),
    );
  }
}
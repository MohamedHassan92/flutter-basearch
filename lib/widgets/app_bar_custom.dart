part of base_arch;

class AppBarCustom {
  AppBarCustom(this.context,{this.title, this.isBackEnabled,
      this.actions,
      this.backgroundColor,
      this.iconThemeColor,
      this.elevation,
      this.bottom,
    this.logoColor,
  this.onBack,
  this.logoAlignment,
  this.leading,
  this.isDrawerEnabled = true});

  BuildContext context;

  String? title;
  bool? isBackEnabled;
  List<Widget>? actions;
  Color? backgroundColor;
  Color? iconThemeColor;
  double? elevation;
  Widget? bottom;
  MainAxisAlignment? logoAlignment;
  Function? onBack;
  IconButton? leading;
  bool isDrawerEnabled;
  Color? logoColor;

  AppBar appBarCustom() {
    isBackEnabled = isBackEnabled == null ? true : isBackEnabled;

    if (isBackEnabled!) {
      if (actions != null) {
        actions!.add(IconButton(
          icon: Icon(
            Icons.arrow_forward,
            color: iconThemeColor,
          ),
          onPressed: onBack == null ?() {
            Navigator.pop(context);
          }:onBack as void Function()?,
        ));
      } else {
        actions = <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_forward, color: iconThemeColor),
            onPressed: onBack == null ? () {
              Navigator.pop(context);
            }:onBack as void Function()?,
          ),
        ];
      }
    }

    BaseConfig.logger!.v("aaa $actions $backgroundColor bbbbbbbbb ${Colors.transparent}");

    return AppBar(
      centerTitle: true,
      elevation: elevation,
      backgroundColor: backgroundColor == null ? Colors.transparent : backgroundColor,
      bottom: bottom as PreferredSizeWidget?,
      leading: isDrawerEnabled ? leading : null,
      title: Row(
    mainAxisAlignment:MainAxisAlignment.end,
      children: <Widget>[
        Expanded(child:Text(title!,style: BaseS.h5(),))//some lesson content page title is to long so i minimized text size
      ],
    ),
      iconTheme: IconThemeData(
          color: Colors.black,),
      actions: actions,
    );
  }
}

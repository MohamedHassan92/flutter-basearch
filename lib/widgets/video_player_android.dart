part of base_arch;

class VideoPlayerAndroid extends StatefulWidget {
  VideoPlayerAndroid(this.videoUrl,{this.isOpenedForHelpVideo=false});

  String videoUrl;
  bool isOpenedForHelpVideo;

  @override
  _VideoPlayerAndroidState createState() => _VideoPlayerAndroidState();
}

class _VideoPlayerAndroidState extends State<VideoPlayerAndroid> {
  var _data;
  AppBar? appBarCustom;

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  late BetterPlayerController _betterPlayerController;
  late Widget playerWidget;

  @override
  void initState() {
    super.initState();
    Wakelock.enable();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);



    widget.videoUrl = widget.videoUrl.replaceAll("http:","https:");

    BetterPlayerDataSource betterPlayerDataSource = BetterPlayerDataSource(
        BetterPlayerDataSourceType.network,
        widget.videoUrl);
    _betterPlayerController = BetterPlayerController(
      BetterPlayerConfiguration(
        autoPlay: true,
        fullScreenAspectRatio: 16 / 9,
        fullScreenByDefault: false,
        allowedScreenSleep: false,

        controlsConfiguration: BetterPlayerControlsConfiguration(
            enableFullscreen: false,
            enableMute: false,
            enableOverflowMenu: false),
        autoDetectFullscreenDeviceOrientation: false,
      ),
        betterPlayerDataSource: betterPlayerDataSource,);


    playerWidget = AspectRatio(
      aspectRatio: 16 / 9,
      child: BetterPlayer(

        controller: _betterPlayerController,
      ),
    );
  }


  @override
  void dispose() {
    super.dispose();
    _betterPlayerController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: Stack(children: <Widget>[
        Container(
          height: double.infinity,
          alignment: Alignment.center,
          child: Container(
            alignment: Alignment.center,
            // margin: EdgeInsets.only(top: D.de),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                playerWidget,
              ],
            ),
          ),
        ),
      ]),
    );
  }
}

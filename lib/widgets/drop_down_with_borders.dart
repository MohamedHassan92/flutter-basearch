part of base_arch;

Widget dropDownItem<T>(
    {required List<DropdownMenuItem<T>> itemList,
    Widget? icon,
    MaterialColor? color,
    Color? borderColor,
    double? borderWidth,
    MaterialColor? iconDisplayColor,
    VoidCallback? onChange<T>(currentSelection)?,
    dynamic value,
    DropdownButtonBuilder? selectedItem,
    VoidCallback? onTap,
    String? dropdownError,
    Decoration? decoation,
    bool isValidate = true}) {
  return Container(
    child: Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB (BaseD.default_10, 0, BaseD.default_10, 0),
          width: double.infinity,
          decoration: decoation==null?BoxDecoration(
            color: color == null ? Colors.white : color,
            border: isValidate
                ? Border.all(color: borderColor??Colors.grey, width:borderWidth?? 1)
                : Border.all(color: Colors.red, width: 1),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(8),
          ):decoation,
          child: DropdownButton<T>(
            selectedItemBuilder: selectedItem,
            onTap: onTap,
            icon: icon,
            value: value,
            hint: Container(
              padding: EdgeInsets.fromLTRB (BaseD.default_10, 0, BaseD.default_10, 0),
            ),
            iconDisabledColor:
                iconDisplayColor == null ? Colors.white70 : iconDisplayColor,
            items: itemList,
            onChanged: (currentSelection) {
              onChange!(currentSelection);
            },
            underline: SizedBox(),
            isDense: false,
            isExpanded: true,
          ),
        ),
        isValidate
            ? SizedBox.shrink()
            : Container(
                width: double.infinity,
                margin: EdgeInsets.only(left: BaseD.default_40),
                child: Text(
                  dropdownError ?? "",
                  style: TextStyle(color: Colors.red),
                ),
              ),
      ],
    ),
  );
}

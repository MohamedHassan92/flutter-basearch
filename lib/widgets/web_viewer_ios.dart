import 'dart:collection';
import 'package:base_arch_project/config/base_arch_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';

class WebViewerIos extends StatefulWidget {
  String? url, asset;

  WebViewerIos({this.url, this.asset = ""});

  @override
  _WebViewerIosState createState() => _WebViewerIosState();
}

class _WebViewerIosState extends State<WebViewerIos> {
  var TAG = "_WebViewerIosState";

  bool _isLoading = false;


  @override
  void initState() {
    super.initState();

  }

  @override
  void dispose() {
    // Every listener should be canceled, the same should be done with this stream.

    super.dispose();
  }

  final GlobalKey webViewKey = GlobalKey();

  InAppWebViewController? webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));
  double progress = 0;

  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;

    return Scaffold(

        body:Stack(children: [
      Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: [
            InAppWebView(
              key: webViewKey,
              // contextMenu: contextMenu,
              initialUrlRequest:
              URLRequest(url: Uri.parse(widget.url!)),
              // initialFile: "assets/index.html",
              initialUserScripts: UnmodifiableListView<UserScript>([]),
              initialOptions: options,
              onWebViewCreated: (controller) {
                webViewController = controller;
                //webViewController?.loadUrl(urlRequest: URLRequest(url: url));
              },
              onLoadStart: (controller, url) {
                widget.url = url.toString();
              },
              androidOnPermissionRequest: (controller, origin, resources) async {
                return PermissionRequestResponse(
                    resources: resources,
                    action: PermissionRequestResponseAction.GRANT);
              },
              shouldOverrideUrlLoading: (controller, navigationAction) async {
                var uri = navigationAction.request.url!;

                if (![
                  "http",
                  "https",
                  "file",
                  "chrome",
                  "data",
                  "javascript",
                  "about"
                ].contains(uri.scheme)) {
                  if (await canLaunch(widget.url!)) {
                    // Launch the App
                    await launch(
                      widget.url!,
                    );
                    // and cancel the request
                    return NavigationActionPolicy.CANCEL;
                  }
                }

                return NavigationActionPolicy.ALLOW;
              },
              onLoadStop: (controller, url) async {
                widget.url = url.toString();
              },
              onLoadError: (controller, url, code, message) {
                BaseConfig.logger!.v("inAppWebView : error : $code : $message");
              },
              onProgressChanged: (controller, progress) {
                BaseConfig.logger!.v("inAppWebView : progress : $progress");
                setState(() {
                  this.progress = progress / 100;
                });
              },
              onUpdateVisitedHistory: (controller, url, androidIsReload) {
                widget.url = url.toString();
              },
              onConsoleMessage: (controller, consoleMessage) {
                BaseConfig.logger!.v(consoleMessage);
              },
            ),
            progress < 1.0
                ? LinearProgressIndicator(value: progress)
                : Container(),
          ],
        )),
    ]));
  }
}

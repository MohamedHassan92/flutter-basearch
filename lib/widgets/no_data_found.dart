part of base_arch;

class NoDataFound extends StatelessWidget {
  const NoDataFound({Key? key, this.receivedMessage}) : super(key: key);
  final String? receivedMessage;

  @override
  Widget build(BuildContext context) {
    var messageToShow =
        (receivedMessage == null) ? tr("no_data_found") : receivedMessage!;
    return InkWell(
      onTap: () {
        BaseConfig.logger!.v("progress : onTap");
      },
      child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Center(
              child: Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(messageToShow, style: BaseS.h2()))))),
    );
  }
}

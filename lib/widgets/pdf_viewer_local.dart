import 'package:base_arch_project/config/base_arch_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';

import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PdfViewerLocalUrl extends StatefulWidget {

  final String urlPDFPath;

  const PdfViewerLocalUrl(this.urlPDFPath);

  @override
  _PdfViewerLocalUrlState createState() => _PdfViewerLocalUrlState();
}

class _PdfViewerLocalUrlState extends State<PdfViewerLocalUrl> {

  int? _totalPages = 0;
  int? _currentPage = 0;
  late PDFViewController _pdfViewController;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose(){
    super.dispose;
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return Scaffold(
      resizeToAvoidBottomInset:false,
      body: PDFView(
        filePath: widget.urlPDFPath,
        autoSpacing: true,
        enableSwipe: true,
        pageSnap: true,
        swipeHorizontal: true,
        nightMode: false,
        fitEachPage: true,
        fitPolicy: FitPolicy.HEIGHT,
        onError: (e) {
          //Show some error message or UI
          BaseConfig.logger!.v("PDF : Error : $e");
        },
        onRender: (_pages) {
          setState(() {
            _totalPages = _pages;
          });
        },
        onViewCreated: (PDFViewController vc) {
          setState(() {
            _pdfViewController = vc;
          });
        },
        onPageChanged: (int? page, int? total) {
          setState(() {
            _currentPage = page;
          });
        },
        onPageError: (page, e) {
          BaseConfig.logger!.v("PDF : Error : $e");
        },
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.chevron_left),
            iconSize: 50,
            color: Colors.black,
            onPressed: () {
              setState(() {
                if (_currentPage! > 0) {
                  _currentPage = _currentPage! - 1;
                  _pdfViewController.setPage(_currentPage!);
                }
              });
            },
          ),
          Text(
            "${_currentPage! + 1}/$_totalPages",
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
          IconButton(
            icon: Icon(Icons.chevron_right),
            iconSize: 50,
            color: Colors.black,
            onPressed: () {
              setState(() {
                if (_currentPage! < _totalPages! - 1) {
                  _currentPage = _currentPage! + 1;
                  _pdfViewController.setPage(_currentPage!);
                }
              });
            },
          ),
        ],
      ),
    );
  }
}
part of base_arch;

class TextFieldWithBorder extends StatelessWidget {
  TextFieldWithBorder(
      {this.hint,
        this.obsecure = false,
        this.passwordText = false,
        this.validator,
        this.onSaved,
        this.errorText,
        this.onTap,
        this.onChanged,
        this.onIconeObSecureClick,
        this.keyboardType,
        this.controller,
        this.textStyle,
        this.hintStyle,
        this.icon ,
        this.onIconTap ,
        this.enabled ,
        this.textDirection,
        this.textInputAction,
        this.onEditingComplete,
        this.onSubmitted ,
        this.enabledBorder ,
        this.focusedBorder ,
        this.border ,
      this.maxLines,
      this.minLines=1});

  final FormFieldSetter<String>? onSaved;
  final String? hint;
  final String? errorText;
  final bool? obsecure;
  final bool? passwordText;
  final TextEditingController? controller;
  final FormFieldValidator<String>? validator;
  final Function? onTap;
  final Function? onIconTap;
  final bool? enabled;
  final Function? onChanged;
  final Function? onIconeObSecureClick;
  final TextInputType? keyboardType;
  final TextStyle? textStyle;
  final TextStyle? hintStyle;
  final IconData? icon;
  final TextDirection? textDirection ;
  final  TextInputAction? textInputAction;
  final Function? onEditingComplete ;
  final Function? onSubmitted ;
  final InputBorder? enabledBorder ;
  final InputBorder? focusedBorder ;
  final InputBorder? border ;
  final int? maxLines ;
  final int? minLines ;
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap:onTap as void Function()?,
        child:Stack(
          alignment: AlignmentDirectional.topStart,
          children: <Widget>[
            TextFormField(
              maxLines: maxLines,
              minLines:minLines??1 ,
              textDirection: textDirection,
              textInputAction:textInputAction,
              onEditingComplete:onEditingComplete as void Function()?,
              onFieldSubmitted:onSubmitted as void Function(String)?,
              keyboardType:
              keyboardType == null ? TextInputType.text : keyboardType,
              controller: controller,
              onSaved: onSaved,
              //onTap: onTap,
              enabled: enabled,
              onChanged: onChanged as void Function(String)?,
              validator: validator,
              autofocus: false,
              enableInteractiveSelection: onTap == null,
              focusNode: onTap != null ? FocusNode(canRequestFocus: false) : null,
              obscureText: obsecure??false,

              style: textStyle,
              //textAlign: TextAlign.center,
              decoration:  InputDecoration(
                filled: true,
                fillColor: Colors.white,
                isDense: false,
                contentPadding: EdgeInsetsDirectional.only(start: icon!=null?40:10, end: 10),
                errorMaxLines: 2,
                labelStyle: TextStyle(fontSize: 15, color: Colors.grey[300]),
                labelText: hint,
                errorText: errorText,
                hintStyle: hintStyle,
                suffixIcon:(passwordText??false)?IconButton(
                    icon: Icon((obsecure??false) ? Icons.visibility_off : Icons.visibility),
                    onPressed: onIconeObSecureClick as void Function()?
                ): null,
                enabledBorder:enabledBorder?? OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Colors.grey,
                    width: 2,
                  ),
                ),

                focusedBorder: focusedBorder?? OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Colors.grey,
                    width: 2,
                  ),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Colors.red,
                    width: 2,
                  ),
                ),
                border:border??OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Colors.grey,
                    width: 2,
                  ),
                ),
              ),
            ),
            icon!=null? Padding(
              padding: EdgeInsets.all(10),
              child: InkWell(
                child: Icon(
                  icon ?? AntDesign.mail,
                  color: Colors.grey[400],
                ),
                onTap: onIconTap as void Function()?,
              ),
            ):Container()
          ],
        )
    );

  }
}
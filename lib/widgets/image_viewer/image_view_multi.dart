part of base_arch;

class ImageViewerMulti extends StatefulWidget {
  static String TAG = "ImageViewer";

  List<String> imageString;
  int index;

  ImageViewerMulti(this.imageString, {this.index = 0});

  @override
  _ImageViewerMultiState createState() => _ImageViewerMultiState();
}

class _ImageViewerMultiState extends State<ImageViewerMulti> {
  var TAG = "_ImageViewerMultiState";

  @override
  void initState() {
    super.initState();

    pageController = PageController(initialPage: widget.index);
  }

  @override
  void dispose() {
    super.dispose();
  }

  PageController? pageController;

  @override
  Widget build(BuildContext context){
    return BaseScreen(
        "",
      isBackEnabled: false,
      body: Container(
        color:Colors.black,
        child: PageView.builder(
          itemCount: widget.imageString.length,
          controller: pageController,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return PinchZoom(
              image: AspectRatio(
                  aspectRatio: 1 / 1,
                  child: TransitionImage(widget.imageString[index],
                      fit: BoxFit.contain)),
              zoomedBackgroundColor: Colors.black.withOpacity(0.5),
              resetDuration: const Duration(milliseconds: 100),
              maxScale: 2.0,
              onZoomStart: (){BaseConfig.logger!.v('Start zooming');},
              onZoomEnd: (){BaseConfig.logger!.v('Stop zooming');},
            );
          },
        ),
      ),
    );
  }
}

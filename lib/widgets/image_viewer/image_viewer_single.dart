part of base_arch;

class ImageVieweSingle extends StatefulWidget {
  final String image;

  ImageVieweSingle(this.image);

  @override
  _ImageVieweSingleState createState() => _ImageVieweSingleState();
}

class _ImageVieweSingleState extends State<ImageVieweSingle> {
  var TAG = "_ImageVieweSingleState";

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return BaseScreen(
        "",
      isBackEnabled: false,
      title: "",
      body: Container(
          color:Colors.black,
          child: Center(
            child: ImageViewerMulti([widget.image],index:0),
          )),
    );
  }
}

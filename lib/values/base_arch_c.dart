part of base_arch;

class BaseC {
  static const skyBlueLite = Color(0xFFE7F4FC);
  static const skyBlue = Color(0xFF00b7f3);
  static const skyBlueDark = Color(0xFF01A6DC);
  static const blueBtn = Color(0xFF3f63ed);
  static const mostardaBtn = Color(0xFFffa500);
  static const greenDark = Color(0xFF008000);
  static const blackLight=Color(0xFF696969);
}

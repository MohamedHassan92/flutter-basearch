part of base_arch;

class BaseS {
  static String defaultFont = "Arial";

  static void setDefaultFont(String font){
    defaultFont = font;
  }

  static h1({Color? color, bool underline = false,TextDecoration? decoration,Color? decorationColor,double? decorationThickness, Color? shadowColor ,String? font}) =>
      TextStyle(
          fontFamily: font??defaultFont,
          shadows: shadowColor == null ? null : shadow(color: shadowColor),
          color: color == null ? Colors.black : color,
          decoration:decoration!=null?decoration: underline ? TextDecoration.underline : null,
          decorationColor:decorationColor!=null?decorationColor:Colors.black ,
          decorationThickness:decorationThickness,
          fontSize: BaseD.h1);

  static h2({Color? color, bool underline = false, TextDecoration? decoration,Color? decorationColor,double? decorationThickness,Color? shadowColor,String? font}) =>
      TextStyle(
          fontFamily: font??defaultFont,
          shadows: shadowColor == null ? null : shadow(color: shadowColor),
          color: color == null ? Colors.black : color,
          decoration:decoration!=null?decoration: underline ? TextDecoration.underline : null,
          decorationColor:decorationColor!=null?decorationColor:Colors.black ,
          decorationThickness:decorationThickness,
          fontSize: BaseD.h2);

  static h3({Color? color, bool underline = false,TextDecoration? decoration, Color? decorationColor,double? decorationThickness,Color? shadowColor,String? font}) =>
      TextStyle(
          fontFamily:font?? defaultFont,
          shadows: shadowColor == null ? null : shadow(color: shadowColor),
          color: color == null ? Colors.black : color,
          decoration:decoration!=null?decoration: underline ? TextDecoration.underline : null,
          decorationColor:decorationColor!=null?decorationColor:Colors.black ,
          decorationThickness:decorationThickness,
          fontSize: BaseD.h3);

  static h4({Color? color, bool underline = false,TextDecoration? decoration,Color? decorationColor,double? decorationThickness, Color? shadowColor,String? font}) =>
      TextStyle(
          fontFamily:font?? defaultFont,
          shadows: shadowColor == null ? null : shadow(color: shadowColor),
          color: color == null ? Colors.black : color,
          decoration:decoration!=null?decoration: underline ? TextDecoration.underline : null,
          decorationColor:decorationColor!=null?decorationColor:Colors.black ,
          decorationThickness:decorationThickness,
          fontSize: BaseD.h4);

  static h5({Color? color, bool underline = false,TextDecoration? decoration,Color? decorationColor,double? decorationThickness, Color? shadowColor,String? font}) =>
      TextStyle(
          fontFamily:font?? defaultFont,
          shadows: shadowColor == null ? null : shadow(color: shadowColor),
          color: color == null ? Colors.black : color,
          decoration:decoration!=null?decoration: underline ? TextDecoration.underline : null,
          decorationColor:decorationColor!=null?decorationColor:Colors.black ,
          decorationThickness:decorationThickness,
          fontSize: BaseD.h5);

  static h6({Color? color, bool underline = false, TextDecoration? decoration,Color? decorationColor,double? decorationThickness,Color? shadowColor,String? font}) =>
      TextStyle(
          fontFamily: font??defaultFont,
          shadows: shadowColor == null ? null : shadow(color: shadowColor),
          color: color == null ? Colors.black : color,
          decoration:decoration!=null?decoration: underline ? TextDecoration.underline : null,
          decorationColor:decorationColor!=null?decorationColor:Colors.black ,
          decorationThickness:decorationThickness,
          fontSize: BaseD.h6);

  static shadow({Color? color}) => [
        Shadow(
          blurRadius: 2.0,
          color: color == null ? Colors.black : color,
          offset: Offset(1.0, 1.0),
        ),
      ];
}

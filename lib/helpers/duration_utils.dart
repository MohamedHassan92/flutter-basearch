part of base_arch;

class DurationUtils{
  static String TAG = "DurationUtils";

   static final int SECOND = 1000;
   static final int MINUTE = 60000;
   static final int HOUR = 3600000;
   static final int DAY = 86400000;
   static final int MONTH = 2629800000;
   static final int YEAR = 31557600000;

  int milliseconds = 0;
  int seconds = 0;
  int minutes = 0;
  int hours = 0;
  int days = 0;
  int months = 0;
  int years = 0;

  DurationUtils(int durationInMillis) {


    if (durationInMillis > YEAR) {
      this.years = (durationInMillis / YEAR).floor();
      durationInMillis %= YEAR;
    }

    if (durationInMillis > MONTH) {
      this.months = (durationInMillis / MONTH).floor();
      durationInMillis %= MONTH;
    }

    if (durationInMillis > DAY) {
      this.days = (durationInMillis / DAY).floor();
      durationInMillis %= DAY;
    }

    if (durationInMillis > HOUR) {
      this.hours = (durationInMillis / HOUR).floor();
      durationInMillis %= HOUR;
    }

    if (durationInMillis > MINUTE) {
      this.minutes = (durationInMillis / MINUTE).floor();
      durationInMillis %= MINUTE;
    }

    if (durationInMillis > SECOND) {
      this.seconds = (durationInMillis / SECOND).floor();
      durationInMillis %= SECOND;
    }

    this.milliseconds = durationInMillis;
  }

  String getFormattedDuration(){
    return "$years y: $months M: $days d: $hours h: $minutes m: $seconds s: $milliseconds SS";
  }

  static String? getTimeAgo(int timestamp){
    int durationInMillis = DateTime.now().millisecondsSinceEpoch-timestamp;
    BaseConfig.logger!.v("$TAG : durationInMillis : $durationInMillis");
    BaseConfig.logger!.v("$TAG : timestamp : $timestamp");
    BaseConfig.logger!.v("$TAG : calendar : ${DateTime.now().millisecondsSinceEpoch}");
    if(durationInMillis>DurationUtils.DAY) {
      DurationUtils durationUtils = new DurationUtils(durationInMillis);
      if (durationUtils.years > 0) {
        return sprintf(tr("n_year_ago"),[durationUtils.years]);
      } else if (durationUtils.months > 0) {
        return sprintf(tr("n_month_ago"),[durationUtils.months]);
      } else if (durationUtils.days > 0) {
        return sprintf(tr("n_day_ago"),[durationUtils.days]);
      } else if (durationUtils.hours > 0) {
        return sprintf(tr("n_hours_ago"),[durationUtils.hours]);
      } else if (durationUtils.minutes > 0) {
        return sprintf(tr("n_minute_ago"),[durationUtils.minutes]);
      } else if (durationUtils.seconds > 0) {
        return sprintf(tr("n_second_ago"),[durationUtils.seconds]);
      } else {
        return tr("now");
      }
    }else{
      return DateFormat("hh:mm a").format(DateTime.fromMillisecondsSinceEpoch(timestamp).toUtc());
    }
  }

  static String getDurationHHMM(int timeInSeconds){
    return DateFormat("HH:mm").format(DateTime.fromMillisecondsSinceEpoch(timeInSeconds * 1000).toUtc());
  }
}
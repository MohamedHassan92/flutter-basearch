part of base_arch;

class DeviceUtils {
  static Future<DeviceInfoModel> getDeviceInfo() async {
    DeviceInfoModel deviceInfoModel = DeviceInfoModel.newDeviceInfoModel();

    if (Platform.isAndroid) {
      var androidInfo = await DeviceInfoPlugin().androidInfo;
      var release = androidInfo.version.release;
      var sdkInt = androidInfo.version.sdkInt;
      var manufacturer = androidInfo.manufacturer;
      var model = androidInfo.model;

      deviceInfoModel.operatingSystem = "android";
      deviceInfoModel.operatingSystemVersion = "$release (SDK $sdkInt)";
      deviceInfoModel.manufacturer = manufacturer;
      deviceInfoModel.model = model;
    }

    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      var systemName = iosInfo.systemName;
      var version = iosInfo.systemVersion;
      var name = iosInfo.name;
      var model = iosInfo.model;

      deviceInfoModel.operatingSystem = systemName;
      deviceInfoModel.operatingSystemVersion = version;
      deviceInfoModel.manufacturer = name;
      deviceInfoModel.model = model;
    }
    deviceInfoModel.id = await PlatformDeviceId.getDeviceId;

    return deviceInfoModel;
  }
}

class DeviceInfoModel extends Object {

  String? operatingSystem ="";
  String? operatingSystemVersion ="";
  String? manufacturer = "";
  String? model  = "";
  String? id  = "";

  DeviceInfoModel(
      this.operatingSystem,
      this.operatingSystemVersion,
      this.manufacturer,
      this.model,
      this.id
      );

  DeviceInfoModel.newDeviceInfoModel();
}

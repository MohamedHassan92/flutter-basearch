import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrefsUtils{

  static SharedPreferences? prefs;
  static Map<String, dynamic> defaults = {};

  static initPrefs() async {
    if(prefs == null) prefs = await SharedPreferences.getInstance();
  }

  static Future<String> getString(String key,{String defaultValue = ""}) async {
    await initPrefs();
    if(!prefs!.containsKey(key)) return defaultValue;
    return prefs!.getString(key)!;
  }

  static Future<int> getInt(String key,{int defaultValue = 0}) async {
    await initPrefs();
    if(!prefs!.containsKey(key)) return defaultValue;
    return prefs!.getInt(key)!;
  }

  static Future<double> getDouble(String key,{double defaultValue = 0}) async {
    await initPrefs();
    if(!prefs!.containsKey(key)) return defaultValue;
    return prefs!.getDouble(key)!;
  }

  static Future<bool> getBool(String key,{bool defaultValue = false}) async {
    await initPrefs();
    if(!prefs!.containsKey(key)) return defaultValue;
    return prefs!.getBool(key)!;
  }
}
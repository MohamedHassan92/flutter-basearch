part of base_arch;

class UrlLauncherUtils {
  static void openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  static void callPhone(String phone) async {
    var url = "tel:$phone";
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  static void smsPhone(String phone) async {
    var url = "sms:$phone";
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  static void mailTo(String email) async {
    var url = "mailto:$email";
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  static void openMap(double latitude, double longitude) async {
    String googleUrl = 'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    }
  }
}

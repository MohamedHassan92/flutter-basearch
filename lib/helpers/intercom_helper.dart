part of base_arch;

class IntercomHelper {
  static void logout() {
    Intercom.instance.logout();
  }

  static Future<void> initializeIntercomMessenger(
      String appId, String androidApiKey, String iosApiKey) async {
    WidgetsFlutterBinding.ensureInitialized();
    await Intercom.instance.initialize(
      appId,
      androidApiKey: androidApiKey,
      iosApiKey: iosApiKey,
    );

    if (BaseConfig.IS_GMS_AVAILABLE) {
      final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
      String token = await _firebaseMessaging.getToken() ?? "";

      if (token.isNotEmpty) await Intercom.instance.sendTokenToIntercom(token);
    }
  }

  static Future<void> createChatWithIntercomMessenger(
    String email, {
    String? name,
    String? phone,
    String? userId,
    Map<String, dynamic>? customAttributes,
  }) async {
    await Intercom.instance.logout();

    String? deviceId = "";
    if(userId == null || userId.isEmpty){
      deviceId = await PlatformDeviceId.getDeviceId;
    }
    String id = userId ?? deviceId ?? "";
    await Intercom.instance.loginIdentifiedUser(userId: id,statusCallback: IntercomStatusCallback(onSuccess: (){
      print("[Intercom][loginIdentifiedUser][Id:$id][Success]");
    },onFailure: (error){
      print("[Intercom][loginIdentifiedUser][Id:$id][Error][$error]");
    }));

    await Intercom.instance.updateUser(
        email: email,
        name: name,
        phone: phone,
        userId: userId,
        customAttributes: customAttributes);
    await Intercom.instance.displayMessenger();
  }

  static Future<bool> handleIntercomPush(Map<String, dynamic> data) async {
    if (await Intercom.instance.isIntercomPush(data)) {
      await Intercom.instance.handlePush(data);
      return true;
    }
    return false;
  }
}

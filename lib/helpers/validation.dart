import 'package:easy_localization/easy_localization.dart' hide TextDirection;
  class Validation{
  bool _checkEmailFormat(String email) {
    RegExp emailRegExp = RegExp(
      r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
      caseSensitive: false,
      multiLine: false,
    );

    return !emailRegExp.hasMatch(email);
  }
  String? stringValidator(String pass, String error) {
    if (pass.isEmpty)
      return error;
    else
      return null;
  }

  String? confirmPasswordValidaitor(String pass,String confirmPassword) {
    if (pass.isEmpty)
      return tr("enter_confirm_password");
    else if (pass != confirmPassword)
      return tr("passwords_dont_match");
    else
      return null;
  }
  String? emailValidator(String email) {
    if (email.isEmpty)
      return tr('empty_email');
    else if (_checkEmailFormat(email))
      return tr('wrong_email_format');
    else
      return null;
  }
  String? emailCorrectIfNotEmpty(String email) {
    if (email.isNotEmpty) {
      if (_checkEmailFormat(email)) {
        return tr('wrong_email_format');
      }
      else {
        return null;
      }
    }
    else
      return tr('empty_email');
  }

  String? passwordComplexValidation(String value ,String errorEmpty , String complexPassword){
    String  pattern = r'^(?=.*\d).{8,}$';
    RegExp regExp = new RegExp(pattern);
  bool isMatched=   regExp.hasMatch(value) ;
     if(value!=null && value.isNotEmpty){
       if(isMatched){
         return null;
       }else{
         return complexPassword;
       }
     }else {
       return errorEmpty;
     }
     }
  }

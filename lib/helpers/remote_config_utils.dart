import 'package:firebase_remote_config/firebase_remote_config.dart';

import 'package:base_arch_project/config/base_arch_config.dart';

class RemoteConfigUtils {

  static RemoteConfig? remoteConfig;
  static Map<String, dynamic> remoteConfigDefaults = {};

  static setDefaults(Map<String, dynamic> defaults) {
    remoteConfigDefaults = defaults;
  }

  static initRemoteConfig() async {
    if (remoteConfig == null) remoteConfig = await RemoteConfig.instance;

    await remoteConfig!.setConfigSettings(
        RemoteConfigSettings(fetchTimeout: Duration(seconds: 20),
            minimumFetchInterval: Duration(hours: 6)));
    await remoteConfig!.setDefaults(remoteConfigDefaults);
  }

  static Future<bool> fetchRemoteConfigValues() async {
    await initRemoteConfig();
    bool isRemoteFetch = false;
    await remoteConfig!.fetchAndActivate().then((value) {
      remoteConfig!.getAll().forEach((key, value) {
        BaseConfig.logger!.v("RemoteConfig : $key => $value}");
      });
      isRemoteFetch = true;
    }, onError: (error) {
      if(error != null) BaseConfig.logger!.v("fetchRemoteConfigValues : $error");
      else BaseConfig.logger!.v("fetchRemoteConfigValues : null");
      isRemoteFetch = false;
    });

    return isRemoteFetch;
  }

  static Future<bool> getBool(String key) async {
    await initRemoteConfig();
    return remoteConfig!.getBool(key);
  }

  static Future<String> getString(String key) async {
    await initRemoteConfig();
    return remoteConfig!.getString(key);
  }

  static Future<int> getInt(String key) async {
    await initRemoteConfig();
    return remoteConfig!.getInt(key);
  }
}
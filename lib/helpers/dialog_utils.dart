part of base_arch;

class DialogUtils {
  static alert1Btn(
    BuildContext context, {
    String? title,
    String? content,
    String? dismissTitle,
    Function? onDismissPressed,
    bool isDismissible = true,
  }) {
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
            onWillPop: isDismissible ? _onWillPop : _onWillNotPop,
            child: AlertDialog(
              title: title != null
                  ? Text(
                      title,
                      style: BaseS.h1(),
                    )
                  : Container(),
              content: Text(
                content!,
                style: BaseS.h2(),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child: Text(
                      dismissTitle == null ? tr("dismiss") : dismissTitle,
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: onDismissPressed == null
                      ? () => Navigator.of(context).pop()
                      : onDismissPressed as void Function()?,
                )
              ],
            ));
      },
    );
  }

  static Future<bool> _onWillPop() async {
    return true;
  }

  static Future<bool> _onWillNotPop() async {
    return false;
  }

  static alert1BtnScrollable(BuildContext context,
      {String? title,
      String? content,
      String? dismissTitle,
      Function? onDismissPressed,
      bool isDismissible = true}) {
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
            onWillPop: isDismissible ? _onWillPop : _onWillNotPop,
            child: AlertDialog(
              title: title != null
                  ? Text(
                      title,
                      style: BaseS.h1(),
                    )
                  : Container(),
              content: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Text(
                    content!,
                    style: BaseS.h2(),
                  )),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child: Text(
                      dismissTitle == null ? tr("dismiss") : dismissTitle,
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: onDismissPressed == null
                      ? () => Navigator.of(context).pop()
                      : onDismissPressed as void Function()?,
                )
              ],
            ));
      },
    );
  }

  static alert2Btns(BuildContext context,
      {String? title,
      String? content,
      String? okTitle,
      Function? onOkPressed,
      Function? onCancelPressed,
      bool isToExit = false,
      bool isDismissible = true}) {
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
            onWillPop: isDismissible ? _onWillPop : _onWillNotPop,
            child: AlertDialog(
              title: title != null
                  ? Text(
                      title,
                      style: BaseS.h1(),
                    )
                  : Container(),
              content: Text(
                content!,
                style: BaseS.h2(),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child: new Text(okTitle ?? tr("ok"),
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: onOkPressed as void Function()?,
                ),
                FlatButton(
                  child: new Text(tr("cancel"),
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: onCancelPressed != null
                      ? onCancelPressed as void Function()?
                      : () {
                          Navigator.of(context).pop();
                          if (isToExit) Navigator.of(context).pop();
                        },
                )
              ],
            ));
      },
    );
  }

  static alert3Btns(BuildContext context,
      {String? title,
      String? content,
      String? okTitle,
      String? thirdTitle,
      Function? onOkPressed,
      Function? onCancelPressed,
      Function? onThirdPressed,
      bool isToExit = false,
      bool isDismissible = true}) {
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
            onWillPop: isDismissible ? _onWillPop : _onWillNotPop,
            child: AlertDialog(
              title: title != null
                  ? Text(
                      title,
                      style: BaseS.h1(),
                    )
                  : Container(),
              content: Text(
                content!,
                style: BaseS.h2(),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child: new Text(okTitle ?? tr("ok"),
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: onOkPressed as void Function()?,
                ),
                FlatButton(
                  child: new Text(thirdTitle ?? tr("login_history"),
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: onThirdPressed as void Function()?,
                ),
                FlatButton(
                  child: new Text(tr("cancel"),
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: onCancelPressed != null
                      ? onCancelPressed as void Function()?
                      : () {
                          Navigator.of(context).pop();
                          if (isToExit) Navigator.of(context).pop();
                        },
                )
              ],
            ));
      },
    );
  }

  static Future alert2BtnsText(BuildContext context,
      {String? content,
      String? okTitle,
      bool isToExit = false,
      String? hint,
      bool isDismissible = true,
      TextInputType? keyboardType = TextInputType.multiline}) async {
    // flutter defined function
    TextEditingController controller = TextEditingController();
    GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    bool _autoValidate = false;

    return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
            onWillPop: isDismissible ? _onWillPop : _onWillNotPop,
            child: AlertDialog(
              content: Form(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        content!,
                        style: BaseS.h2(),
                      ),
                      Container(height: BaseD.default_10),
                      TextFieldWithBorder(
                          hint: hint,
                          textStyle: BaseS.h2(),
                          maxLines: 1,
                          minLines: 1,
                          keyboardType: keyboardType,
                          hintStyle: BaseS.h2(color: Colors.grey),
                          controller: controller,
                          validator: (String? desc) {
                            if (desc!.isEmpty)
                              return sprintf(tr("enter_s"), [hint]);
                            else
                              return null;
                          })
                    ],
                  )),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child: new Text(okTitle ?? tr("ok"),
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      Navigator.of(context).pop(controller.text);
                    }
                  },
                ),
                FlatButton(
                  child: new Text(tr("cancel"),
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
      },
    );
  }

  static Future alert2BtnsText_MTM(BuildContext context,
      {String? content,
      TextEditingController? controller,
      String? okTitle,
      String? exitTitle,
      String? thirdBtnTitle,
      Function? onOkPressed,
      Function? onCancelPressed,
      Function? onThirdBtnPressed,
      TextInputType? keyboardType,
      String Function(String?)? validator,
      bool isToExit = false,
      String? hint,
      bool isDismissible = true}) async {
    // flutter defined function

    GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    bool _autoValidate = false;

    List<Widget> actions = [
      // usually buttons at the bottom of the dialog
      FlatButton(
        child: new Text(okTitle ?? tr("ok"),
            style: BaseS.h3(color: BaseC.mostardaBtn)),
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            onOkPressed!();
          }
        },
      ),
      FlatButton(
        child: new Text(exitTitle ?? tr("cancel"),
            style: BaseS.h3(color: BaseC.mostardaBtn)),
        onPressed: onCancelPressed != null
            ? onCancelPressed as void Function()?
            : () {
                Navigator.of(context).pop();
              },
      )
    ];

    if (thirdBtnTitle != null) {
      actions.add(FlatButton(
        child:
            new Text(thirdBtnTitle, style: BaseS.h3(color: BaseC.mostardaBtn)),
        onPressed: onThirdBtnPressed != null
            ? onThirdBtnPressed as void Function()?
            : () {
                Navigator.of(context).pop();
              },
      ));
    }

    return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
            onWillPop: isDismissible ? _onWillPop : _onWillNotPop,
            child: AlertDialog(
              content: Form(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        content!,
                        style: BaseS.h2(),
                      ),
                      Container(height: BaseD.default_10),
                      TextFieldWithBorder(
                          hint: hint,
                          textStyle: BaseS.h2(),
                          maxLines: 1,
                          minLines: 1,
                          keyboardType: keyboardType == null
                              ? TextInputType.multiline
                              : keyboardType,
                          hintStyle: BaseS.h2(color: Colors.grey),
                          controller: controller,
                          validator: validator)
                    ],
                  )),
              actions: actions,
            ));
      },
    );
  }

  static Future alert2BtnsTextRate(BuildContext context,
      {String? content,
      String? okTitle,
      bool isToExit = false,
      String? hint,
      bool isDismissible = true}) async {
    // flutter defined function
    TextEditingController controller = TextEditingController();
    GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    bool _autoValidate = false;

    Map<String, dynamic> map = {};

    return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
            onWillPop: isDismissible ? _onWillPop : _onWillNotPop,
            child: AlertDialog(
              content: Form(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        content!,
                        style: BaseS.h2(),
                      ),
                      Container(height: BaseD.default_10),
                      RatingBar(
                        initialRating: 0,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: false,
                        itemCount: 5,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        ratingWidget: RatingWidget(
                            full: Icon(
                              Icons.star,
                              color: BaseC.mostardaBtn,
                            ),
                            empty: Icon(
                              Icons.star,
                              color: Colors.grey,
                            ),
                            half: Icon(
                              Icons.star_half,
                              color: BaseC.mostardaBtn,
                            )),
                        onRatingUpdate: (rating) {
                          map['rate'] = rating;
                        },
                      ),
                      Container(height: BaseD.default_10),
                      TextFieldWithBorder(
                          hint: hint,
                          textStyle: BaseS.h2(),
                          maxLines: 1,
                          minLines: 1,
                          keyboardType: TextInputType.multiline,
                          hintStyle: BaseS.h2(color: Colors.grey),
                          controller: controller,
                          validator: (String? desc) {
                            if (desc!.isEmpty)
                              return sprintf(tr("enter_s"), [hint]);
                            else
                              return null;
                          })
                    ],
                  )),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child: new Text(okTitle ?? tr("ok"),
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: () {
                    if (_formKey.currentState!.validate() &&
                        map['rate'] != null) {
                      map['text'] = controller.text;
                      Navigator.of(context).pop(map);
                    } else if (map['rate'] == null) {
                      Fluttertoast.showToast(msg: tr("add_rate"));
                    }
                  },
                ),
                FlatButton(
                  child: new Text(tr("cancel"),
                      style: BaseS.h3(color: BaseC.mostardaBtn)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
      },
    );
  }

  static Future alert2BtnsTextImage(BuildContext context,
      {String? content,
      String? okTitle,
      bool isToExit = false,
      String? hint,
      bool isDismissible = true}) async {
    // flutter defined function
    TextEditingController controller = TextEditingController();
    GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    bool _autoValidate = false;

    List<Asset> images = [];
    String loadAssetsError = "";

    Map<String, dynamic> map = {};

    return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
            onWillPop: isDismissible ? _onWillPop : _onWillNotPop,
            child: StatefulBuilder(builder: (context, setState) {
              return AlertDialog(
                content: Form(
                    key: _formKey,
                    autovalidateMode: AutovalidateMode.disabled,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        content != null
                            ? Text(
                                content,
                                style: BaseS.h2(),
                              )
                            : Container(),
                        content != null
                            ? Container(height: BaseD.default_10)
                            : Container(),
                        TextFieldWithBorder(
                            hint: hint,
                            textStyle: BaseS.h2(),
                            maxLines: 1,
                            minLines: 1,
                            keyboardType: TextInputType.multiline,
                            hintStyle: BaseS.h2(color: Colors.grey),
                            controller: controller,
                            validator: (String? desc) {
                              if (desc!.isEmpty)
                                return sprintf(tr("enter_s"), [hint]);
                              else
                                return null;
                            }),
                        Container(height: BaseD.default_10),
                        Container(
                          height: BaseD.default_150,
                          child: ButtonTheme(
                            minWidth: double.infinity,
                            height: double.infinity,
                            padding: EdgeInsets.zero,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: BaseC.mostardaBtn, width: 2),
                                borderRadius:
                                    BorderRadius.circular(BaseD.default_5),
                              ),
                              onPressed: () async {
                                BaseConfig.logger!.v("onPressed");
                                loadAssetsError = "";

                                try {
                                  images = await MultiImagePicker.pickImages(
                                    maxImages: 1,
                                    enableCamera: true,
                                    selectedAssets: images,
                                    cupertinoOptions:
                                        CupertinoOptions(takePhotoIcon: "chat"),
                                    materialOptions: MaterialOptions(
                                      actionBarColor:
                                          "#${BaseC.skyBlue.value.toRadixString(16).substring(2)}",
                                      actionBarTitle: "adwaa",
                                      allViewTitle: tr("all_photos"),
                                      useDetailsView: false,
                                      selectCircleStrokeColor: "#000000",
                                    ),
                                  );
                                } on Exception catch (e) {
                                  loadAssetsError = e.toString();
                                }

                                setState(() {});
                              },
                              color: Colors.white,
                              child: images.isEmpty
                                  ? Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.image,
                                          color: BaseC.mostardaBtn,
                                          size: BaseD.default_40,
                                        ),
                                        Text(tr("question_picture"),
                                            style: BaseS.h2(
                                                color: BaseC.mostardaBtn)),
                                      ],
                                    )
                                  : Card(
                                      margin: EdgeInsets.zero,
                                      color: Colors.white,
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(
                                                  BaseD.default_5))),
                                      clipBehavior: Clip.hardEdge,
                                      child: Container(
                                        height: double.infinity,
                                        width: double.infinity,
                                        alignment: Alignment.center,
                                        child: AssetThumb(
                                          asset: images[0],
                                          height: 1000,
                                          width: 1000,
                                        ),
                                      ),
                                    ),
                            ),
                          ),
                        ),
                      ],
                    )),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  FlatButton(
                    child: new Text(okTitle ?? tr("ok"),
                        style: BaseS.h3(color: BaseC.mostardaBtn)),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        map['text'] = controller.text;
                        if (images != null && images.isNotEmpty)
                          map['image'] = images[0];
                        Navigator.of(context).pop(map);
                      }
                    },
                  ),
                  FlatButton(
                    child: new Text(tr("cancel"),
                        style: BaseS.h3(color: BaseC.mostardaBtn)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            }));
      },
    );
  }
}

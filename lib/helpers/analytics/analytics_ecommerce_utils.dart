part of base_arch;

class AnalyticsEcommerceUtils {
  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  static Future<void> addToCart(AnalyticsEventItem product,
      {String currency = "EGP"}) async {
    await analytics.logAddToCart(
      currency: currency,
      value: product.price?.toDouble(),
      items: [product],
    );
  }

  static Future<void> removeFromCart(AnalyticsEventItem product,
      {String currency = "EGP"}) async {
    await analytics.logRemoveFromCart(
      currency: currency,
      value: product.price?.toDouble(),
      items: [product],
    );
  }

  static Future<void> beginCheckout(
      List<AnalyticsEventItem> cartList, double totalAmount,
      {String currency = "EGP", String? promocode,Map<String, Object?>? parameters}) async {

    List<String> itemsArray = [];
    cartList.forEach((element) {
      itemsArray.add(element.itemId ?? "");
    });

    Map<String, Object?> params = {
      'currency': currency,
      'value': totalAmount,
      'coupon': promocode,
      'items': itemsArray.join(","),
    };

    if(parameters != null){
      parameters.keys.forEach((key){
        params[key] = parameters[key];
      });
    }

    await analytics.logEvent(
        name: 'checkout',
        parameters: params
    );
  }

  static Future<void> logPurchaseSuccess(List<AnalyticsEventItem> cartList,
      double totalAmount, String transactionId, String paymentMethod,
      {String currency = "EGP", String? promocode, Map<String, Object?>? parameters}) async {
    List<String> itemsArray = [];
    cartList.forEach((element) {
      itemsArray.add(element.itemId ?? "");
    });

    Map<String, Object?> params = {
      'transaction_id': transactionId,
      'affiliation': paymentMethod,
      'currency': currency,
      'value': totalAmount,
      'coupon': promocode,
      'items': itemsArray.join(","),
    };

    if(parameters != null){
      parameters.keys.forEach((key){
        params[key] = parameters[key];
      });
    }

    await analytics.logEvent(
      name: 'purchase_success',
      parameters: params
    );
  }

  static Future<void> logPurchaseFailed(List<AnalyticsEventItem> cartList,
      double totalAmount, String transactionId, String paymentMethod,
      {String currency = "EGP", String? promocode}) async {

    List<String> itemsArray = [];
    cartList.forEach((element) {
      itemsArray.add(element.itemId ?? "");
    });

    Map<String, dynamic>? params = {
      'transactionId': transactionId,
      'affiliation': paymentMethod,
      'currency': currency,
      'value': totalAmount,
      'coupon': promocode,
      'items': itemsArray.join(","),
    };

    await analytics.logEvent(name: 'purchase_failed', parameters: params);
  }

  static Future<void> logPurchasePending(List<AnalyticsEventItem> cartList,
      double totalAmount, String transactionId, String paymentMethod,
      {String currency = "EGP", String? promocode}) async {

    List<String> itemsArray = [];
    cartList.forEach((element) {
      itemsArray.add(element.itemId ?? "");
    });

    Map<String, dynamic>? params = {
      'transactionId': transactionId,
      'affiliation': paymentMethod,
      'currency': currency,
      'value': totalAmount,
      'coupon': promocode,
      'items': itemsArray.join(","),
    };

    await analytics.logEvent(name: 'purchase_pending', parameters: params);
  }
}

class EcommerceItem {
  String? itemId;
  String? itemName;
  double? price;

  EcommerceItem(this.itemId, this.itemName, this.price);
}

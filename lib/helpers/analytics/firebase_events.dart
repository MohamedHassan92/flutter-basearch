import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

class FirebaseEventsMessage{


  static void showEventLog(String screenName)async{
    final FirebaseAnalytics analytics = FirebaseAnalytics.instance;

    final FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: analytics);
    await analytics.logEvent(name: screenName);
  }


}
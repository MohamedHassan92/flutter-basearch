part of base_arch;

class AnalyticsRoute{
  String screenName;
  String screenClass;

  AnalyticsRoute(this.screenName, this.screenClass);
}
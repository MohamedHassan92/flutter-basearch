part of base_arch;

// A Navigator observer that notifies RouteAwares of changes to state of their Route
final routeObserver = RouteObserver<PageRoute>();

mixin RouteAwareAnalytics<T extends StatefulWidget> on State<T>
implements RouteAware {
  AnalyticsRoute get route;

  @override
  void didChangeDependencies() {
    if(ModalRoute.of(context) is PageRoute){
      routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPop() {}

  @override
  void didPopNext() {
    // Called when the top route has been popped off,
    // and the current route shows up.
    _setCurrentScreen(route);
  }

  @override
  void didPush() {
    // Called when the current route has been pushed.
    _setCurrentScreen(route);
  }

  @override
  void didPushNext() {}

  Future<void> _setCurrentScreen(AnalyticsRoute analyticsRoute) {
    BaseConfig.logger!.v('Setting current screen to [${analyticsRoute.screenName}] [${analyticsRoute.screenClass}]');
    return AnalyticsUtils.setCurrentScreen(
      analyticsRoute.screenName,
      analyticsRoute.screenClass,
    );
  }
}
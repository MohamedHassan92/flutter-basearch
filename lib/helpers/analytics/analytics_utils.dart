part of base_arch;

class AnalyticsUtils {

  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  static List<NavigatorObserver> navigatorObservers(){
    return [
      FirebaseAnalyticsObserver(analytics: analytics),
    ];
  }

  static Future<void> logEvent(String name,{Map<String,dynamic>? parameters}) async {
    await analytics.logEvent(
      name: name,
      parameters: parameters ?? {},
    );
  }

  static Future<void> setUserId(String userId) async {
    await analytics.setUserId(id:userId);
  }

  static Future<void> setCurrentScreen(String screenName,String screenClassOverride) async {
    await analytics.setCurrentScreen(
      screenName: screenName,
      screenClassOverride: screenClassOverride,
    );
  }

  static Future<void> setUserProperties(Map<String,String> properties) async {
    properties.forEach((key, value) async {
      await analytics.setUserProperty(name: key, value: value);
    });

  }
}
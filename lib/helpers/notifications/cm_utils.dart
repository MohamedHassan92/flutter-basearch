part of base_arch;

class CmUtils {

  static Future init() async {
    if(BaseConfig.IS_GMS_AVAILABLE){
      return FcmUtils.init();
    }else if(BaseConfig.IS_HMS_AVAILABLE){
      return HcmUtils.init();
    }else{
      return true;
    }
  }

  static Future subscribeToTopic(String topic) async {
    if(BaseConfig.IS_GMS_AVAILABLE){
      return FcmUtils.subscribeToTopic(topic);
    }else if(BaseConfig.IS_HMS_AVAILABLE){
      return HcmUtils.subscribeToTopic(topic);
    }else{
      return true;
    }
  }

  static Future unSubscribeFromTopic(String topic) async {
    if(BaseConfig.IS_GMS_AVAILABLE){
      return FcmUtils.unSubscribeFromTopic(topic);
    }else if(BaseConfig.IS_HMS_AVAILABLE){
      return HcmUtils.unSubscribeFromTopic(topic);
    }else{
      return true;
    }
  }

  static Future unSubscribeFromAllTopics({bool fromApi = false}) async {
    if(BaseConfig.IS_GMS_AVAILABLE){
      return FcmUtils.unSubscribeFromAllTopics(fromApi:fromApi);
    }else if(BaseConfig.IS_HMS_AVAILABLE){
      return HcmUtils.unSubscribeFromAllTopics();
    }else{
      return true;
    }
  }

  static Future setSubscribedTopics(List<String> userSubscriptions) async {
    if(BaseConfig.IS_GMS_AVAILABLE){
      return FcmUtils.setSubscribedTopics(userSubscriptions);
    }else if(BaseConfig.IS_HMS_AVAILABLE){
      return HcmUtils.setSubscribedTopics(userSubscriptions);
    }else{
      return true;
    }
  }

  static Future<List<String>> getSubscribedTopics({bool fromApi = false}) async {
    if(BaseConfig.IS_GMS_AVAILABLE){
      return FcmUtils.getSubscribedTopics(fromApi:fromApi);
    }else if(BaseConfig.IS_HMS_AVAILABLE){
      return HcmUtils.getSubscribedTopics();
    }else{
      return [];
    }
  }


  static Future<bool> sendPushNotification(FcmEndPointBody body) async {
    if(BaseConfig.IS_GMS_AVAILABLE){
      return FcmUtils.sendPushNotification(body);
    }else if(BaseConfig.IS_HMS_AVAILABLE){
      return true;
    }else{
      return true;
    }
  }
}

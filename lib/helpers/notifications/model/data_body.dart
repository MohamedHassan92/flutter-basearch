import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(explicitToJson: true)
class DataBody extends Object {

  @JsonKey(name:"notificationType")
  String? notificationType;
  @JsonKey(name:"notificationData")
  String? notificationData;
  @JsonKey(name:"title")
  String? title;
  @JsonKey(name:"body")
  String? body;
  @JsonKey(name:"android_channel_id")
  String? android_channel_id;


  DataBody(this.notificationType, this.notificationData, this.title, this.body,
      this.android_channel_id);

  factory DataBody.fromJson(Map<String, dynamic> json) {
    return DataBody(
      json['notificationType'] as String?,
      json['notificationData'] as String?,
      json['title'] as String?,
      json['body'] as String?,
      json['android_channel_id'] as String?,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'notificationType': this.notificationType,
    'notificationData': this.notificationData,
    'title': this.title,
    'body': this.body,
    'android_channel_id': this.android_channel_id,
    'click_action': 'FLUTTER_NOTIFICATION_CLICK'
  };
}
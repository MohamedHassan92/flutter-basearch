import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(explicitToJson: true)
class NotificationBody extends Object {

  @JsonKey(name:"title")
  String? title;
  @JsonKey(name:"body")
  String? body;
  @JsonKey(name:"android_channel_id")
  String? android_channel_id;
  @JsonKey(name:"sound")
  String? sound = "default";


  NotificationBody(this.title,
      this.body,
      this.android_channel_id,
      this.sound);

  factory NotificationBody.fromJson(Map<String, dynamic> json) {
    return NotificationBody(
      json['title'] as String?,
      json['body'] as String?,
      json['android_channel_id'] as String?,
      json['sound'] as String?,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'title': this.title,
    'body': this.body,
    'android_channel_id': this.android_channel_id,
    'sound': this.sound ?? "default",
  };
}
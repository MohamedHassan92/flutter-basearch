import 'package:json_annotation/json_annotation.dart';

import 'data_body.dart';
import 'notification_body.dart';

@JsonSerializable(explicitToJson: true)
class FcmEndPointBody extends Object {
  @JsonKey(name: "to")
  String? to;

  @JsonKey(name: "data")
  DataBody? data;

  FcmEndPointBody(this.to, this.data);

  factory FcmEndPointBody.fromJson(Map<String, dynamic> json) {
    return FcmEndPointBody(
      json['to'] as String?,
      json['data'] == null
          ? null
          : DataBody.fromJson(json['data'] as Map<String, dynamic>),
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'to': "/topics/${this.to}",
        'data': this.data?.toJson(),
        'notification': (NotificationBody(this.data!.title, this.data!.body,
                this.data!.android_channel_id, null))
            .toJson()
      };
}

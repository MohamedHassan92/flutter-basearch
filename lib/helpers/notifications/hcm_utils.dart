part of base_arch;

class HcmUtils {
  static SharedPreferences? prefs;

  static String PREF_USER_SUBSCRIBED_HCM_TOPICS = "HCM_SUBSCRIBED_TOPICS";

  static Future init() async {
    WidgetsFlutterBinding.ensureInitialized();

    if (prefs == null) prefs = await SharedPreferences.getInstance();
    subscribedTopicsList.clear();
  }

  static List<String> subscribedTopicsList = [];

  static Future subscribeToTopic(String topic) async {
    await init();

   /* await pushkit.Push.subscribe(topic).then((value){
      BaseConfig.logger!.v("subscribeToTopic : $topic");
      if (!subscribedTopicsList.contains(topic))
        subscribedTopicsList.add(topic);
    }, onError: (error) {
      BaseConfig.logger!.v("subscribeToTopic : error : ${error.toString()}");
    });*/

    await prefs!.setStringList(
        PREF_USER_SUBSCRIBED_HCM_TOPICS, subscribedTopicsList);
  }

  static Future unSubscribeFromTopic(String topic) async {
    await init();

    List<String> subscribedTopicsList = await getSubscribedTopics();

   /* await  pushkit.Push.unsubscribe(topic).then((value) {
      BaseConfig.logger!.v("unsubscribeFromTopic : $topic");
      subscribedTopicsList.remove(topic);
    }, onError: (error) {
      BaseConfig.logger!.v("unsubscribeFromTopic : error : ${error.toString()}");
    });*/

    await prefs!.setStringList(
        PREF_USER_SUBSCRIBED_HCM_TOPICS, subscribedTopicsList);
  }

  static Future unSubscribeFromAllTopics() async {
    await init();
    List<String> subscribedTopicsList = await getSubscribedTopics();
    if (subscribedTopicsList != null && subscribedTopicsList.isNotEmpty) {
      for (int i = 0; i < subscribedTopicsList.length; i++) {
        var topic = subscribedTopicsList[i];
       /* await pushkit.Push.unsubscribe(topic).then((value) {
          BaseConfig.logger!.v("unsubscribeFromTopic : $topic");
        }, onError: (error) {
          BaseConfig.logger!.v("unsubscribeFromTopic : error : ${error.toString()}");
        });*/
      }
    }else{
      BaseConfig.logger!.v("unsubscribeFromTopic : error : ");
    }
    subscribedTopicsList.clear();
    List<String> clearList = [];

    prefs!.setStringList(
        PREF_USER_SUBSCRIBED_HCM_TOPICS, clearList);
  }

  static Future setSubscribedTopics(List<String> userSubscriptions) async {
    await init();
    await prefs!.setStringList(
        PREF_USER_SUBSCRIBED_HCM_TOPICS, userSubscriptions);
  }

  static Future<List<String>> getSubscribedTopics() async {
    await init();
    List<String>? subscribedTopicsList = prefs!.getStringList(
        PREF_USER_SUBSCRIBED_HCM_TOPICS);
    if(subscribedTopicsList==null)
      subscribedTopicsList = [];
    return subscribedTopicsList;
  }
}

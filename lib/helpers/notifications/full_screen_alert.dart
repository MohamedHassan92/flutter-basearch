part of base_arch;

class FullScreenAlert extends StatefulWidget {
  Map<String, dynamic> message;

  FullScreenAlert(this.message);

  @override
  _FullScreenAlertstate createState() => _FullScreenAlertstate();
}

class _FullScreenAlertstate extends State<FullScreenAlert> {
  String TAG = "FullScreenAlert";

  @override
  void initState() {
    super.initState();
    BaseConfig.logger!.v("$TAG : initState");

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      var title;
      var content;

      if (widget.message.containsKey('notification')) {
        title = widget.message['notification']['title'].toString();
        content = widget.message['notification']['body'].toString();
      } else {
        title = widget.message['title'].toString();
        content = widget.message['body'].toString();
      }

      DialogUtils.alert2Btns(context, title: title, content: content,
          onOkPressed: () {
        onSelectNotification(json.encode(widget.message));
      }, onCancelPressed: () {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
        /*Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => SplashScreen()),
                ModalRoute.withName('/'));*/
      });
    });
  }

  Future onSelectNotification(String payload) async {
    BaseConfig.onPushNotificationClicked(payload);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    BaseConfig.logger!.v("$TAG : build");
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.white,
          width: double.infinity,
          height: double.infinity,
        )
      ],
    );
  }
}

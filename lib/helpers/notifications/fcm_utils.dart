part of base_arch;

class FcmUtils {
  static SharedPreferences? prefs;

  static String PREF_USER_SUBSCRIBED_FCM_TOPICS = "FCM_SUBSCRIBED_TOPICS";

  static Future init() async {
    WidgetsFlutterBinding.ensureInitialized();

    if (prefs == null) prefs = await SharedPreferences.getInstance();
    subscribedTopicsList.clear();
  }

  static List<String> subscribedTopicsList = [];

  static Future subscribeToTopic(String topic) async {
    await init();


    await FirebaseMessaging.instance.subscribeToTopic(topic).then((value) {
      BaseConfig.logger!.v("subscribeToTopic : $topic");
      if (!subscribedTopicsList.contains(topic))
        subscribedTopicsList.add(topic);
    }, onError: (error) {
      BaseConfig.logger!.v("subscribeToTopic : error : ${error.toString()}");
    });

    await prefs!.setStringList(
        PREF_USER_SUBSCRIBED_FCM_TOPICS, subscribedTopicsList);
  }

  static Future unSubscribeFromTopic(String topic) async {
    await init();

    List<String> subscribedTopicsList = await getSubscribedTopics();

    await FirebaseMessaging.instance.unsubscribeFromTopic(topic).then((value) {
      BaseConfig.logger!.v("unsubscribeFromTopic : $topic");
      subscribedTopicsList.remove(topic);
    }, onError: (error) {
      BaseConfig.logger!.v("unsubscribeFromTopic : error : ${error.toString()}");
    });

    await prefs!.setStringList(
        PREF_USER_SUBSCRIBED_FCM_TOPICS, subscribedTopicsList);
  }

  static Future unSubscribeFromAllTopics({bool fromApi = false}) async {
    await init();
    List<String> subscribedTopicsList = await getSubscribedTopics(
        fromApi: fromApi);
    if (subscribedTopicsList != null && subscribedTopicsList.isNotEmpty) {
      for (int i = 0; i < subscribedTopicsList.length; i++) {
        var topic = subscribedTopicsList[i];

        await Future.delayed(Duration(milliseconds: 100)); // throttle due to 3000 QPS limit
        unawaited(FirebaseMessaging.instance.unsubscribeFromTopic(topic)); // import pedantic for unawaited
        BaseConfig.logger!.v("unsubscribeFromTopic : $topic");

      }
    } else {
      BaseConfig.logger!.v("unsubscribeFromTopic : error : ");
    }
    subscribedTopicsList.clear();
    List<String> clearList = [];

    prefs!.setStringList(
        PREF_USER_SUBSCRIBED_FCM_TOPICS, clearList);
  }

  static Future setSubscribedTopics(List<String> userSubscriptions) async {
    await init();
    await prefs!.setStringList(
        PREF_USER_SUBSCRIBED_FCM_TOPICS, userSubscriptions);
  }

  static Future<List<String>> getSubscribedTopics({bool fromApi = false}) async {
    await init();
    List<String>? subscribedTopicsList;
    subscribedTopicsList =
    fromApi ? await getSubscribedTopicsFromApi() : prefs!.getStringList(
        PREF_USER_SUBSCRIBED_FCM_TOPICS);

    BaseConfig.logger!.v("subscribedTopicsList : $subscribedTopicsList");
    if (subscribedTopicsList == null)
      subscribedTopicsList = [];
    return subscribedTopicsList;
  }

  static Future<List<String>> getSubscribedTopicsFromApi() async {
    List<String> subscribedTopicsList = [];

    String token = await FirebaseMessaging.instance.getToken() ?? "";
    BaseConfig.logger!.v("getSubscribedTopicsFromApi : token : $token");

    final url = 'https://iid.googleapis.com/iid/info/$token?details=true';
    final headers = {
      'content-type': 'application/json',
      'Authorization': "key=AIzaSyDLvaGIQWRMmC2Q35Maaj-OrqSTRMK_6vs",
    };
    final response = await BaseDioUtils.request(BaseDioUtils.REQUEST_GET,
        url,
        headers: headers);

    if (response != null && response!.data != null &&
        (response!.data! as Map<String, dynamic>).containsKey("rel") &&
        (response!.data!["rel"] as Map<String, dynamic>).containsKey(
            "topics")) {
      BaseConfig.logger!.v("getSubscribedTopicsFromApi : subscribedTopics");
      Map<String, dynamic> subscribedTopics = response!.data!['rel']['topics'];
      subscribedTopics.keys.forEach((element) {
        subscribedTopicsList.add(element);
      });
    } else {
      BaseConfig.logger!.v("getSubscribedTopicsFromApi : null");
    }

    prefs!.setStringList(PREF_USER_SUBSCRIBED_FCM_TOPICS, subscribedTopicsList);

    return subscribedTopicsList;
  }

  static Future<bool> sendPushNotification(FcmEndPointBody body) async {
    final postUrl = 'https://fcm.googleapis.com/fcm/send';

    final headers = {
      'content-type': 'application/json',
      'Authorization': "key=AIzaSyDLvaGIQWRMmC2Q35Maaj-OrqSTRMK_6vs",
    };

    final response = await http.post(
        Uri.parse(postUrl),
        body: jsonEncode(body.toJson()),
        encoding: Encoding.getByName('utf-8'),
        headers: headers);
    if (response.statusCode == 200) {
      // on success do sth
      BaseConfig.logger!.v('TEST ok push FCM');
      BaseConfig.logger!.v('TEST ok push FCM $response');
      return true;
    } else {
      BaseConfig.logger!.v('FCM error');
      // on failure do sth
      return false;
    }
  }
}

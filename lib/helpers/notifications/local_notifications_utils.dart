part of base_arch;

class LocalNotificationsUtils {
  late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  Function? onDidRecieveLocalNotification;
  Function? onSelectNotification;
  BuildContext? context;
  String? TAG;

  LocalNotificationsUtils(Function onDidRecieveLocalNotification, Function onSelectNotification) {
    this.flutterLocalNotificationsPlugin =
        new FlutterLocalNotificationsPlugin();
    this.onDidRecieveLocalNotification = onDidRecieveLocalNotification;
    this.onSelectNotification = onSelectNotification;
    this.init();
  }

  init() {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('app_icon_notification');
    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: this.onDidRecieveLocalNotification as Future<dynamic> Function(int, String?, String?, String?)?);
    var initializationSettings = new InitializationSettings(
        android:initializationSettingsAndroid, iOS:initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: this.onSelectNotification as Future<dynamic> Function(String?)?);
  }

  Future showNotification(String channel_id,String channel_name,String channel_desc,String title,String body,String paylod) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        channel_id, channel_name, channelDescription: channel_desc,
        importance: Importance.max, priority: Priority.high,styleInformation: BigTextStyleInformation(''));
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android:androidPlatformChannelSpecifics, iOS:iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, title, body, platformChannelSpecifics,
        payload: paylod).then((value){
          BaseConfig.logger!.v("showNotification : success");
    },onError: (error){
      BaseConfig.logger!.v("showNotification : error : ${error.toString()}");
    });
  }

/*  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugBaseConfig.logger!.v('onSelectNotification payload: ' + payload);
    }

  }

  Future onDidRecieveLocalNotification(int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    if (payload != null) {
      debugBaseConfig.logger!.v('onDidRecieveLocalNotification payload: ' + payload);
    }


  }*/
}

part of base_arch;

class Utils {
  static double devicePixelRatio() => MediaQueryData().devicePixelRatio;

  static double deviceWidthInPixel(BuildContext context) =>
      MediaQuery.of(context).size.width;

  static double deviceHeightInPixel(BuildContext context) =>
      MediaQuery.of(context).size.height;

  static double roundDouble(double value, int places) {
    double mod = pow(10.0, places) as double;
    return ((value * mod).round().toDouble() / mod);
  }

  static bool isArabic(){
    return BaseConfig.SELECTED_LOCALE.languageCode == "ar";
  }

  static String? getYoutubeThumbnail(String url){
    return sprintf("https://img.youtube.com/vi/%s/hqdefault.jpg",[getYoutubeVideoId(url)]);
  }

  static String? getYoutubeVideoId(String url){
    if(url == null || url.isEmpty) return "";

    String pattern = "^.*((youtu.be\\/)|(v\\/)|(\\/u\\/\\w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*";
    final regex = RegExp(pattern, caseSensitive: false, multiLine: false);

    if (regex.hasMatch(url)) {
      final videoId = regex.firstMatch(url)!.group(7);
      return videoId;
    } else {
      return "";
    }
  }
  static String formattedNumber(int numberToFormat) {
    var _formattedNumber = NumberFormat.compactCurrency(
      decimalDigits: 2,
      symbol:
      '', // if you want to add currency symbol then pass that in this else leave it empty.
    ).format(numberToFormat);

    return _formattedNumber;
  }

  static openFileViewer(String fileUrl) async {
    if (await canLaunch(fileUrl)) {
      await launch(fileUrl);
    } else {
      throw 'Could not open file.';
    }
  }

  static void printLongLine(String text) {
    final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => BaseConfig.logger!.v(match.group(0)));
  }

  static Future<bool> checkPermission(Permission permission) async {
    if (_checkPlatform(permission)) {
      return Future.value(true);
    }
    var permissionStatus = await permission.request();
    if (permissionStatus.isGranted) {
      return Future.value(true);
    } else {
      return Future.value(false);
    }
  }

  static bool _checkPlatform(Permission permission) {
    if (Platform.isIOS) {
      return (permission == Permission.unknown ||
          permission == Permission.phone ||
          permission == Permission.sms ||
          permission == Permission.storage);
    } else {
      return (permission == Permission.unknown ||
          permission == Permission.mediaLibrary ||
          permission == Permission.photos ||
          permission == Permission.reminders||
          permission == Permission.storage);
    }
  }
  static bool containsIgnoreCase(String string1, String string2) {
    return string1.toLowerCase().contains(string2.toLowerCase());
  }


  static Map<String, String> getParametersFromUrl(String url) {
    var uri = Uri.parse(url);
    uri.queryParameters.forEach((k, v) {
      BaseConfig.logger!.v('key: $k - value: $v');
    });
    return uri.queryParameters;
  }

  static bool isEnglish(text) {
    RegExp pattern = RegExp(r'^[a-zA-Z0-9. -_?]*$');
    var result = pattern.hasMatch(text);
    return result;
  }

}
